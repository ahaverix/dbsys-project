<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="java.util.ArrayList,java.util.List,de.tum.in.dbpra.model.bean.InventoryShopBean,de.tum.in.dbpra.model.bean.WristbandBean,java.util.regex.Pattern;"
    %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 




<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team 3's Music Festival</title>
        <meta name="Team 3's Music Festival" content="Team 3's Music Festival">
        <link rel="stylesheet" href="./resources/css/custom.css">
    </head>
    <body>
    <%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.html");
		} else
			user = (String) session.getAttribute("user");
		int loginID = -1;
		if (session.getAttribute("loginID") == null) {
			response.sendRedirect("login.html");
		} else {
			 loginID = Integer.valueOf((Integer) session.getAttribute("loginID"));
			 System.out.println(loginID);
			//loginID = Integer.parseInt(s);
		}
		
		String userEmail = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userEmail = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		}
	%>
        <div id="header">
            </div>
        <div class= "bodyDiv">
			<a href="LoginSuccess.jsp">
            	<img id="logo" alt="Logo" src="./resources/images/logo.jpg" width=100%/>
			</a>	
        </div>
		<nav>
			<div class ="vertline">
				<table>
					<colgroup width=20%; span="5"></colgroup>
					<tr>
						<td><a class="vertline" href="tickets.html">Tickets</a></td>
						<td><a class="vertline" href="timetable.html">My Timetable</a></td>
						<td><a class="vertline" href="staff.jsp">Festival Staff</a></td>
						<td></td>
						<td>Logged in: <%=user %> </td>
						<td><form action="LogoutServlet" method="post"><input type="submit" value="Logout" ></form></td>
					</tr>
				</table>   
			</div>
		</nav>
		<h2>Team 3's Music Festival - The best festival you will ever visit</h2>
		<br>
		<br>
		<div>

<!-- This branch checks wether a doPost has already been done-->
		<%if(request.getAttribute("answer")==null){
			
		}
		//This branch checks wether a doPost was successfull by analyzing the answer attribute. If so, a bill of the purchase must be displayed.
		if(request.getAttribute("answer")!=null &&((String)request.getAttribute("answer")).charAt(0)=="P".charAt(0)){
			
				String[] result= ((String) request.getAttribute("answer")).split(Pattern.quote("$"));%>
				<%String[] items = new String[(result.length-2)];  
						for (int i = 0; i < items.length; i++) {
						items[i]=result[(i+1)];
						}%>
				<h3>	<%=result[0]%></h3>
				<br>
				<h3> Bill:</h3>
				<br>
				<table border="1" style="width: 100%">
				<tr>
				<th>Time:</th>
				<th>Merchandise Name:</th>
				<th>Merchandise Price:</th>
				<th>Amount:</th>
				<th>Total Cost:</th>
				</tr>
				<%for(int i=0; i< (items.length)/5; i++){ %>
        			<tr>
            		<td><%=items[(0+(i*5))]%></td>
            		<td><%=items[(1+(i*5))]%></td>
            		<td><%=items[(2+(i*5))]%></td>
            		<td><%=items[(3+(i*5))]%></td>
            		<td><%=items[(4+(i*5))]%></td>
        			</tr>
				<%} %>
        				<tr>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td><label for="SUM"><b>SUM: </b></label><%=result[(result.length-1)]%></td>
        			</tr>
					</table>
					<br>
		<% }
		//This branch gives out the responses of the Trigger if something went wrong. A purchase was not completed in this case. 
		else if(request.getAttribute("answer")!=null){%>
			<h3>${answer}</h3>
			<br>
		<% }%>
		<!-- This branch checks wether a a shop has been specified. If not a shop has to be selected-->
		<%if(request.getParameter("id")==null && request.getAttribute("id")==null){%>
			<h3>Please select the shop in which you are authorized to operate:</h3>
			<div>
				
				<form action="./Purchase" method="get">
				<select class="form-control" name="id" id="id">         
	      			<c:forEach var="shop" items="${shops}">
	          			<option value="${shop.shopID}">${shop.shopName}</option>
	      			</c:forEach>
	   			</select>
	   			

				<input type = "submit"> 
				</form>
				
				</div>
			
		<%}
		else 
			 {%>
		<!-- If a shop has been selected then the user can continue with the Purchase-->
		<h3>Please select the WristbandID of the customer, the Product he would like to buy and the amount:</h3>
			<div>
			
		<form action="./Purchase" method="post">
			
			
			
			<table class="beta" style="border-collapse: collapse;">
			<tr>
				<th width="5%"></th>
				<th width="20%">Merchandise Name</th>
				<th width="15%">Amount</th>
				<th width="15%"></th>
				
			</tr>
			<tbody style="background-color:rgb(230,230,230);">
				<c:forEach var="product" items="${shopProducts}">
			<tr>
	          	<td><input type= "checkbox" name="merchandiseID" value="${product.merchandiseID}" id="merchandiseID"></td>
	          	<td>${product.merchandiseName}</td>
	          	<td><input type = "text" name="amount" maxlength="4" id="amount"></td>
	          	<td> </td>
	        </tr>  	
	      		</c:forEach>
	      	<tbody style="color:white;">
			<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			</tr>
			<tbody>
			<tr>
			<td></td>
			<th><label for="wristbandID">WristbandID:</label></th>
			<td><input type = "text" name="wristbandID" maxlength="9"  id="wristbandID"></td>
			<td><input type="submit" value="Submit Selection" /></td></td>
			</tr>		
			</table>
			<input type="hidden" name="id" value="${id}"/>
			</form>
			
			<br>
			<br>
			<br>
				<br>
				<br>
				<%} %>
	

			</div>

		
		</div>
		<br>

		<br>
		<br>
        <div class ="vertline">
			<table>
				<colgroup width=33%; span="3"></colgroup>
				<tr>
					<td>Team 3's Music Festival</td>
					<td>Praktikum Datenbanksysteme SS 16</td>
					<td>Team 3</td>
				</tr>
            </table>   
		</div>
</head>
<body>