<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="performances" scope="request"
	type="java.util.ArrayList<de.tum.in.dbpra.model.bean.TimetableBean>"></jsp:useBean>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Team 3's Music Festival</title>
<meta name="Team 3's Music Festival" content="Team 3's Music Festival">
<link rel="stylesheet" href="./resources/css/custom.css">
</head>
<body>
	<%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.html");
		} else
			user = (String) session.getAttribute("user");
		int loginID = -1;
		if (session.getAttribute("loginID") == null) {
			response.sendRedirect("login.html");
		} else {
			loginID = Integer.valueOf((Integer) session
					.getAttribute("loginID"));
			System.out.println(loginID);
			//loginID = Integer.parseInt(s);
		}

		String userEmail = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userEmail = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		}
	%>
	<div id="header"></div>
	<div class="bodyDiv">
		<a href="LoginSuccess.jsp"> <img id="logo" alt="Logo"
			src="resources/images/logo.jpg" width=100% />
		</a>
	</div>
	<nav>
	<div class="vertline">
		<table>
			<colgroup width=20%; span="5"></colgroup>
			<tr>
				<td><a class="vertline" href="tickets.jsp">Tickets</a></td>
				<td><a class="vertline" href="timetable.jsp">My Timetable</a></td>
				<td><a class="vertline" href="staff.jsp">Festival Staff</a></td>
				<td></td>
				<td>Logged in: <%=user%>
				</td>
				<td><form action="LogoutServlet" method="post">
						<input type="submit" value="Logout">
					</form></td>
			</tr>
		</table>
	</div>
	</nav>
	<h2>Your Timetable</h2>
	<%
			//if there is an error, show it
			if (request.getAttribute("error") != null) {
		%>
	<b><%=request.getAttribute("error")%></b> 
	<br>
	<%	} %>
	<br> This is your Timetable, follow your favorite bands
	everywhere!
	<br> If you want to add or remove a band, edit your timetable
	<a href="./ChooseBand">here</a>!
	<br>
	<br>
	<br>
	<table class="alpha" style="border-collapse: collapse;">
		<tr>
			<th width="5%"></th>
			<th width="15%">Start Time</th>
			<th width="15%">End Time</th>
			<th width="20%">Band Name</th>
			<th width="20%">Location Point</th>			
			<th width="20%">Google Maps</th>			
			<th width="5%"></th>
		</tr>
		<%
			for (de.tum.in.dbpra.model.bean.TimetableBean performance : performances) {
		%>
		<tr>
			<td></td>
			<td><%=performance.getStartPlay()%></td>
			<td><%=performance.getEndPlay()%></td>
			<td><%=performance.getBandName()%></td>
			<td><%=performance.getLocation()%></td>
			<td><a target="_blank" href="http://maps.google.com/maps?q=<%=performance.getLatitude()%>,<%=performance.getLongitude()%>">Go to Maps</a></td>
			<td></td>
		</tr>
		<%
			}
		%>
	</table>
	<br>
	<br>
	<br>
	<div class="vertline">
		<table>
			<colgroup width=33%; span="3"></colgroup>
			<tr>
				<td>Team 3's Music Festival</td>
				<td>Praktikum Datenbanksysteme SS 16</td>
				<td>Team 3</td>
			</tr>
		</table>
	</div>
</body>
</html>