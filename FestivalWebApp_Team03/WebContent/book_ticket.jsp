<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team 3's Music Festival</title>
        <meta name="Team 3's Music Festival" content="Team 3's Music Festival">
        <link rel="stylesheet" href="./resources/css/custom.css">
    </head>
    <body>
	<%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.html");
		} else
			user = (String) session.getAttribute("user");
		int loginID = -1;
		if (session.getAttribute("loginID") == null) {
			response.sendRedirect("login.html");
		} else {
			 loginID = Integer.valueOf((Integer) session.getAttribute("loginID"));
			 System.out.println(loginID);
			//loginID = Integer.parseInt(s);
		}
		String loginEmail= "";
		if (session.getAttribute("email") == null) {
			response.sendRedirect("login.html");
		} else {
			 loginEmail = (String) session.getAttribute("email");
		}
		String userEmail = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userEmail = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		}
	%>
	<div id="header">
            </div>
        <div class= "bodyDiv">
			<a href="LoginSuccess.jsp">
            	<img id="logo" alt="Logo" src="resources/images/logo.jpg" width=100%/>
			</a>	
        </div>
		<nav>
			<div class ="vertline">
				<table>
					<colgroup width=20%; span="5"></colgroup>
					<tr>
						<td><a class="vertline" href="tickets.jsp">Tickets</a></td>
						<td><a class="vertline" href="timetable.jsp">My Timetable</a></td>
						<td><a class="vertline" href="staff.jsp">Festival Staff</a></td>
						<td></td>
						<td>Logged in: <%=user %> </td>
						<td><form action="LogoutServlet" method="post"><input type="submit" value="Logout" ></form></td>
					</tr>
				</table>   
			</div>
		</nav>
		<h2>Ticket Booking</h2>
		<% if (request.getAttribute("error") != null) { %>
			<h3 style='color: red;'>Booking unsuccessful!</h3>
			<%= request.getAttribute("error") %>
			<br>
			<br>
			<br>
		<% } %>
		
		<% if (request.getAttribute("ticket") != null) { %>
			<h3 style='color: green;'>Booking successful!</h3>
			<h3>Personalize your ticket <a href="personalize">here</a></h3>

		<% } %>
		<% if (request.getAttribute("visitor") != null) { %>
			<h3 style='color: green;'>Visitor account created!</h3>

		<% } %>

		
		
			<table border="1" style="width:60%" align="center">
	<tr>
		<th></th>
		<th>Concert</th>
		<th>Camping</th>
		<th>Concert & Camping</th>
	</tr>
	<tr>
		<th>Weekend Ticket</th>
		<td>$140</td>
		<td>$80</td>
		<td>$220</td>
	</tr>
	<tr>
		<th>Weekend VIP Ticket</th>
		<td>$170</td>
		<td>$110</td>
		<td>$250</td>
	</tr>
	<tr>
		<th>Single Day Ticket</th>
		<td>$50</td>
		<td>$30</td>
		<td>$80</td>
	</tr>
	<tr>
		<th>Single Day VIP Ticket</th>
		<td>$80</td>
		<td>$60</td>
		<td>$140</td>
	</tr>
	</table>
	<br>

	<h4>Please choose the characteristics your ticket should have.</h4>
		<form method="post" action="booking">
            	<table align="center">
					<tr>
						<td width="20%"></td>
						<th align="left" width="30%"><label for="type">Ticket type*</label></th>
						<td align="left" width="50%"><select name="type">
								<option value="">Select</option>
								<option value="weekend">Weekend</option>
   								 <option value="friday">Friday</option>
   								 <option value="saturday">Saturday</option>
   								 <option value="sunday">Sunday</option>				 
   							</select></td> 
					</tr>
            		<tr>
						<td></td>
						<th align="left"><label for="area">Area*</label></th>
						<td align="left"><input type="radio" name="area" value="both" />Concert & Camping
							<input type="radio" name="area" value="concert" />Concert
							<input type="radio" name="area" value="camping" />Camping</td> 						
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="vip">VIP*</label></th>
						<td align="left"><input type="radio" name="vip" value="1" />Yes
										<input type="radio" name="vip" value="0" />No</td> 
					</tr>
					<tr><td></td><td></td></tr>
				
					
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
				</table>
				<br>
				<input type="submit" value="Book Ticket"/>
			</form>
		<br>
		<br>
        <div class ="vertline">
			<table>
				<colgroup width=33%; span="3"></colgroup>
				<tr>
					<td>Team 3's Music Festival</td>
					<td>Praktikum Datenbanksysteme SS 16</td>
					<td>Team 3</td>
				</tr>
            </table>   
		</div>
    </body>
</html>