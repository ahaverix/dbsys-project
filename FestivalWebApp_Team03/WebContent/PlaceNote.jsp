<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="workstatuses" scope="request" type="java.util.ArrayList<de.tum.in.dbpra.model.bean.WorkstatusBean>"></jsp:useBean>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team 3's Music Festival</title>
        <meta name="Team 3's Music Festival" content="Team 3's Music Festival">
        <link rel="stylesheet" href="./resources/css/custom.css">
    </head>
    <body>
	<%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.html");
		} else
			user = (String) session.getAttribute("user");
		int loginID = -1;
		if (session.getAttribute("loginID") == null) {
			response.sendRedirect("login.html");
		} else {
			 loginID = Integer.valueOf((Integer) session.getAttribute("loginID"));
			 System.out.println(loginID);
			//loginID = Integer.parseInt(s);
		}
		
		String userEmail = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userEmail = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		}
	%>
	<div id="header">
            </div>
        <div class= "bodyDiv">
			<a href="LoginSuccess.jsp">
            	<img id="logo" alt="Logo" src="resources/images/logo.jpg" width=100%/>
			</a>	
        </div>
		<nav>
			<div class ="vertline">
				<table>
					<colgroup width=20%; span="5"></colgroup>
					<tr>
						<td><a class="vertline" href="tickets.jsp">Tickets</a></td>
						<td><a class="vertline" href="timetable.jsp">My Timetable</a></td>
						<td><a class="vertline" href="staff.jsp">Festival Staff</a></td>
						<td></td>
						<td>Logged in: <%=user %> </td>
						<td><form action="LogoutServlet" method="post"><input type="submit" value="Logout" ></form></td>
					</tr>
				</table>   
			</div>
		</nav>
		<h2>Note Placement</h2>
		<% if (request.getAttribute("error") != null) { %>
			<h3 style='color: red;'>Note could not be placed!</h3>
			<%= request.getAttribute("error") %>
			<br>
			<br>
			<br>
		<% } %>
		<form method="post">
			Please enter a content for your note into the text box below
			<br>
			<div>
				<input type="text" name="content" id="content" style='width: 80%;'/>
			</div>
			<br>
			Please select personnels from the table below, that you want to be concerned by the note
			<br>
			<br>
			<table>
				<tr>
					<th width="45%">Personnel Information</th>
					<th width="5%"></th>
					<th width="50%">Shift Information</th>
				</tr>
			</table>
			<table>
	            <tr>
					<th width="5%">ID</th>
					<th width="10%">First Name</th>
					<th width="10%">Last Name</th>
					<th width="10%">Job</th>
					<th width="10%">Experience</th>
					<th width="5%"></th>
					<th width="10%">Start Time</th>
					<th width="10%">End Time</th>
					<th width="20%">Task</th>
					<th width="10%">Location</th>
				</tr>
				<% for (de.tum.in.dbpra.model.bean.WorkstatusBean workstatus : workstatuses){%>
					<tr>
						<td><label><input type="checkbox" name="personnel_ids" value=<%= workstatus.getPersonnel_id() %>><%= workstatus.getPersonnel_id() %></label></td>
						<td><%= workstatus.getPersonnel_first_name() %></td>
						<td><%= workstatus.getPersonnel_last_name() %></td>
						<td><%= workstatus.getPersonnel_job() %></td>
						<td><%= workstatus.getPersonnel_experience() %></td>
						<td></td>
						<% if (workstatus.getShift_start_time() != null){ %>
							<td><%= workstatus.getShift_start_time() %></td>
							<td><%= workstatus.getShift_end_time() %></td>
							<td><%= workstatus.getShift_task() %></td>
							<td><%= workstatus.getShift_location() %></td>
						<% } else{ %>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						<% } %>
					</tr>
				<% } %>
			</table>
			<br>
			<input type="submit" value="post note"/>
		</form>
		<br>
		<br>
		<br>
        <div class ="vertline">
			<table>
				<colgroup width=33%; span="3"></colgroup>
				<tr>
					<td>Team 3's Music Festival</td>
					<td>Praktikum Datenbanksysteme SS 16</td>
					<td>Team 3</td>
				</tr>
            </table>   
		</div>
    </body>
</html>