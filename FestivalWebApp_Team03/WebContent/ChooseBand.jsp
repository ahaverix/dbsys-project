<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="chosenBands" scope="request" type="java.util.ArrayList<de.tum.in.dbpra.model.bean.BandBean>"></jsp:useBean>
<jsp:useBean id="otherBands" scope="request" type="java.util.ArrayList<de.tum.in.dbpra.model.bean.BandBean>"></jsp:useBean>

<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team 3's Music Festival</title>
        <meta name="Team 3's Music Festival" content="Team 3's Music Festival">
        <link rel="stylesheet" href="./resources/css/custom.css">
    </head>
    <body>
	<%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.html");
		} else
			user = (String) session.getAttribute("user");
		int loginID = -1;
		if (session.getAttribute("loginID") == null) {
			response.sendRedirect("login.html");
		} else {
			 loginID = Integer.valueOf((Integer) session.getAttribute("loginID"));
			 System.out.println(loginID);
			//loginID = Integer.parseInt(s);
		}
		
		String userEmail = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userEmail = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		}
	%>
	<div id="header">
            </div>
        <div class= "bodyDiv">
			<a href="LoginSuccess.jsp">
            	<img id="logo" alt="Logo" src="resources/images/logo.jpg" width=100%/>
			</a>	
        </div>
		<nav>
			<div class ="vertline">
				<table>
					<colgroup width=20%; span="5"></colgroup>
					<tr>
						<td><a class="vertline" href="tickets.jsp">Tickets</a></td>
						<td><a class="vertline" href="timetable.jsp">My Timetable</a></td>
						<td><a class="vertline" href="staff.jsp">Festival Staff</a></td>
						<td></td>
						<td>Logged in: <%=user %> </td>
						<td><form action="LogoutServlet" method="post"><input type="submit" value="Logout" ></form></td>
					</tr>
				</table>   
			</div>
		</nav>
		<h2>Bands</h2>
		<% if (request.getAttribute("error") != null) { %>
			<b><%= request.getAttribute("error") %></b>
			<br>
			<br>
		<% } else if (request.getAttribute("Reloaded") != null) { %>
			<b>Selection Successful! View your <a href="./Timetable">Timetable</a>!</b>
			<br>
			<br>
		<% } %>		
		You can see all the performing bands of the music festival in the table below.
		<br>
		The already selected bands are listed on the top of the table.
		<br>
		<br>
		If you want to follow or unfollow a band, select it or deselect it and click on "Submit Selection".
		<br>
		<br>
		<form method="post">
			<input type="hidden" name="Reloaded" value="true" />
			<table class="beta" style="border-collapse: collapse;">
				<tr>
					<th width="5%"></th>
					<th width="10%"></th>
					<th width="20%">Band Name</th>
					<th width="15%"></th>
				</tr>
				<% for (de.tum.in.dbpra.model.bean.BandBean chosenBand : chosenBands){%>
	            	<tr>
	            		<td> </td>
	            		<td><input type="checkbox" name="band_id" value=<%= chosenBand.getBandId() %> checked="checked"></td>
	            		<td><%= chosenBand.getBandName() %></td>
	            		<td> </td>
	            	</tr>
            	<% } %>
				<% for (de.tum.in.dbpra.model.bean.BandBean otherBand : otherBands){%>
	            	<tr>
	            		<td> </td>
	            		<td><input type="checkbox" name="band_id" value=<%= otherBand.getBandId() %>></td>
	            		<td><%= otherBand.getBandName() %></td>
	            		<td> </td>
	            	</tr>
            	<% } %>
			</table>
			<br>
			<br>
			<input type="submit" value="Submit Selection" />
		</form>
		<br>
        <div class ="vertline">
			<table>
				<colgroup width=33%; span="3"></colgroup>
				<tr>
					<td>Team 3's Music Festival</td>
					<td>Praktikum Datenbanksysteme SS 16</td>
					<td>Team 3</td>
				</tr>
            </table>   
		</div>
    </body>
</html>