<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team 3's Music Festival</title>
        <meta name="Team 3's Music Festival" content="Team 3's Music Festival">
        <link rel="stylesheet" href="./resources/css/custom.css">
    </head>
    <body>
        <div id="header">
            </div>
        <div class= "bodyDiv">
			<a href="LoginSuccess.jsp">
            	<img id="logo" alt="Logo" src="./resources/images/logo.jpg" width=100%/>
			</a>	
        </div>
		<nav>
			<div class ="vertline">
				<table>
					<colgroup width=20%; span="5"></colgroup>
					<tr>
						<td><a class="vertline" href="login.html">Tickets</a></td>
						<td><a class="vertline" href="login.html">My Timetable</a></td>
						<td><a class="vertline" href="login.html">Festival Staff</a></td>
						<td></td>
						<td><a class="vertline" href="login.html">Login </a></td>
					</tr>
				</table>   
			</div>
		</nav>
		<h2>Registration</h2>
		<% if (request.getAttribute("error") != null) { %>
			<h3 style='color: red;'>Registration unsuccessful!</h3>
			<%= request.getAttribute("error") %>
			<br>
			<br>
			<br>
		<% } %>
	
		<h4>Please fill in the following form. The fields marked with * are mandatory.</h4>
		<form method="post" action="register">
            	<table align="left">
					<colgroup width=30%; span="4"></colgroup>
            		<tr>
						<td></td>
						<th align="left"><label for="gender">Gender*</label></th>
						<td><input type="radio" name="gender" value="M" />Male
							<input type="radio" name="gender" value="F" />Female</td> 
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="title">Title</label></th>
						<td><select name="title">
								<option value="">Select</option>
   								 <option value="Dr.">Dr.</option>
   								 <option value="Prof.">Prof.</option>
   								 <option value="Prof. Dr.">Prof. Dr.</option>
   							</select></td> 
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="first_name">First name*</label></th>
						<td><input type="text" name="first_name" id="first_name" maxlength="35"/></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="last_name">Last name*</label></th>
						<td><input type="text" name="last_name" id="last_name" maxlength="35"/></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="birthday">Date of birth*</label></th>
						<td>
						<select name="b_day">
								<option value="ss">Day</option>
   								 <option value="01">1</option>
   								 <option value="02">2</option>
   								 <option value="03">3</option>
								 <option value="04">4</option>
   								 <option value="05">5</option>
   								 <option value="06">6</option>
								 <option value="07">7</option>
								 <option value="08">8</option>
   								 <option value="09">9</option>
   								 <option value="10">10</option>
								 <option value="11">11</option>
   								 <option value="12">12</option>
   								 <option value="13">13</option>
								 <option value="14">14</option>
   								 <option value="15">15</option>
   								 <option value="16">16</option>
								 <option value="17">17</option>
								 <option value="18">18</option>
   								 <option value="19">19</option>
   								 <option value="20">20</option>
								 <option value="21">21</option>
   								 <option value="22">22</option>
   								 <option value="23">23</option>
								 <option value="24">24</option>
   								 <option value="25">25</option>
   								 <option value="26">26</option>
								 <option value="27">27</option>
								 <option value="28">28</option>
   								 <option value="29">29</option>
								 <option value="30">30</option>
   								 <option value="31">31</option>
   							</select>
										
						<select name="b_month">
								<option value="ss">Month</option>
   								 <option value="01">Jan</option>
   								 <option value="02">Feb</option>
   								 <option value="03">Mar</option>
								 <option value="04">Apr</option>
   								 <option value="05">May</option>
   								 <option value="06">Jun</option>
								 <option value="07">Jul</option>
   								 <option value="08">Aug</option>
   								 <option value="09">Sep</option>
								 <option value="10">Oct</option>
   								 <option value="11">Nov</option>
   								 <option value="12">Dez</option>
   							</select>
							<select name="b_year">
								<option value="ssss">Year</option>
   								 <option value="2010">2010</option>
   								 <option value="2009">2009</option>
   								 <option value="2008">2008</option>
								 <option value="2007">2007</option>
   								 <option value="2006">2006</option>
   								 <option value="2005">2005</option>
								 <option value="2004">2004</option>
   								 <option value="2003">2003</option>
   								 <option value="2002">2002</option>
								 <option value="2001">2001</option>
   								 <option value="2000">2000</option>
   								 <option value="1999">1999</option>
								 <option value="1998">1998</option>
   								 <option value="1997">1997</option>
   								 <option value="1996">1996</option>
								 <option value="1995">1995</option>
   								 <option value="1994">1994</option>
   								 <option value="1993">1993</option>
								 <option value="1992">1992</option>
   								 <option value="1991">1991</option>
   								 <option value="1990">1990</option>
								 <option value="1989">1989</option>
   								 <option value="1988">1988</option>
   								 <option value="1987">1987</option>
								 <option value="1986">1986</option>
   								 <option value="1985">1985</option>
   								 <option value="1984">1984</option>
   								 <option value="1983">1983</option>
   								 <option value="1982">1982</option>
   								 <option value="1981">1981</option>
								 <option value="1980">1980</option>
   								 <option value="1979">1979</option>
   								 <option value="1978">1978</option>
								 <option value="1977">1977</option>
   								 <option value="1976">1976</option>
   								 <option value="1975">1975</option>
								 <option value="1974">1974</option>
   								 <option value="1973">1973</option>
   								 <option value="1972">1972</option>
   								 <option value="1971">1971</option>
   								 <option value="1970">1970</option>
   								 <option value="1969">1969</option>
								 <option value="1968">1968</option>
   								 <option value="1967">1967</option>
   								 <option value="1966">1966</option>
								 <option value="1965">1965</option>
   								 <option value="1964">1964</option>
   								 <option value="1963">1963</option>
								 <option value="1962">1962</option>
   								 <option value="1961">1961</option>
   								 <option value="1960">1960</option>
   								 <option value="1959">1959</option>
								 <option value="1958">1958</option>
   								 <option value="1957">1957</option>
   								 <option value="1956">1956</option>
								 <option value="1955">1955</option>
   								 <option value="1954">1954</option>
   								 <option value="1953">1953</option>
								 <option value="1952">1952</option>
   								 <option value="1951">1951</option>
   								 <option value="1950">1950</option>
   								 <option value="1949">1949</option>
								 <option value="1948">1948</option>
   								 <option value="1947">1947</option>
   								 <option value="1946">1946</option>
								 <option value="1945">1945</option>
   								 <option value="1944">1944</option>
   								 <option value="1943">1943</option>
								 <option value="1942">1942</option>
   								 <option value="1941">1941</option>
   								 <option value="1940">1940</option>
   								 <option value="1939">1939</option>
								 <option value="1938">1938</option>
   								 <option value="1937">1937</option>
   								 <option value="1936">1936</option>
								 <option value="1935">1935</option>
   								 <option value="1934">1934</option>
   								 <option value="1933">1933</option>
								 <option value="1932">1932</option>
   								 <option value="1931">1931</option>
   								 <option value="1930">1930</option> 
   								 <option value="1929">1929</option>
								 <option value="1928">1928</option>
   								 <option value="1927">1927</option>
   								 <option value="1926">1926</option>
								 <option value="1925">1925</option>
   								 <option value="1924">1924</option>
   								 <option value="1923">1923</option>
								 <option value="1922">1922</option>
   								 <option value="1921">1921</option>
   								 <option value="1920">1920</option>
   								 <option value="1919">1919</option>
								 <option value="1918">1918</option>
   								 <option value="1917">1917</option>
   								 <option value="1916">1916</option>
								 <option value="1915">1915</option>
   								 <option value="1914">1914</option>
   								 <option value="1913">1913</option>
								 <option value="1912">1912</option>
   								 <option value="1911">1911</option>
   								 <option value="1910">1910</option>
   							</select>
						
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="address">Address</label></th>
						<td><input type="text" name="address" id="address" maxlength="60"/></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="phone_no">Phone  number</label></th>
						<td><input type="text" name="phone_no" id="phone_no" maxlength="20"/></td>
						<td></td>
					</tr>
					<tr>
					</tr>	
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr>
						<td></td>
						<th align="left"><label for="credit_card_no">Credit card number*</label></th>
						<td><input type="text" name="credit_card_no" id="credit_card_no" maxlength="16"/></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="exp_date">Expiration date*</label></th>
						<td>
						<select name="e_day">
								<option value="ss">Day</option>
   								 <option value="01">1</option>
   								 <option value="02">2</option>
   								 <option value="03">3</option>
								 <option value="04">4</option>
   								 <option value="05">5</option>
   								 <option value="06">6</option>
								 <option value="07">7</option>
								 <option value="08">8</option>
   								 <option value="09">9</option>
   								 <option value="10">10</option>
								 <option value="11">11</option>
   								 <option value="12">12</option>
   								 <option value="13">13</option>
								 <option value="14">14</option>
   								 <option value="15">15</option>
   								 <option value="16">16</option>
								 <option value="17">17</option>
								 <option value="18">18</option>
   								 <option value="19">19</option>
   								 <option value="20">20</option>
								 <option value="21">21</option>
   								 <option value="22">22</option>
   								 <option value="23">23</option>
								 <option value="24">24</option>
   								 <option value="25">25</option>
   								 <option value="26">26</option>
								 <option value="27">27</option>
								 <option value="28">28</option>
   								 <option value="29">29</option>
								 <option value="30">30</option>
   								 <option value="31">31</option>
   							</select>
										
						<select name="e_month">
								<option value="ss">Month</option>
   								 <option value="01">Jan</option>
   								 <option value="02">Feb</option>
   								 <option value="03">Mar</option>
								 <option value="04">Apr</option>
   								 <option value="05">May</option>
   								 <option value="06">Jun</option>
								 <option value="07">Jul</option>
   								 <option value="08">Aug</option>
   								 <option value="09">Sep</option>
								 <option value="10">Oct</option>
   								 <option value="11">Nov</option>
   								 <option value="12">Dez</option>
   							</select>
							<select name="e_year">
								<option value="ss">Year</option>
   								 <option value="2030">2030</option>
   								 <option value="2029">2029</option>
   								 <option value="2028">2028</option>
								 <option value="2027">2027</option>
   								 <option value="2026">2026</option>
   								 <option value="2025">2025</option>
								 <option value="2024">2024</option>
   								 <option value="2023">2023</option>
   								 <option value="2022">2022</option>
								 <option value="2021">2021</option>
								 <option value="2020">2020</option>
   								 <option value="2019">2019</option>
   								 <option value="2018">2018</option>
   								 <option value="2017">2017</option>
   								 <option value="2016">2016</option>
   							</select>
						
						<td></td>
					</tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr>
						<td></td>
						<th align="left"><label for="is_company">Company*</label></th>
						<td><input type="radio" name="is_company" value="1" />Yes
							<input type="radio" name="is_company" value="0" />No</td> 
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="company_name">Company name</label></th>
						<td><input type="text" name="company_name" id="company_name" maxlength="50"/></td>
						<td></td>
					</tr>
					<tr>
					</tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr>
						<td></td>
						<th align="left"><label for="email">E-mail*</label></th>
						<td><input type="text" name="email" id="email" maxlength="40"/></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="password">Password*</label></th>
						<td><input type="password" name="password" id="password" maxlength="20"/></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<th align="left"><label for="password2">Confirm password*</label></th>
						<td><input type="password" name="password2" id="password2" maxlength="20"/></td>
						<td></td>
					</tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
				</table>
				<br>
				<input type="submit" value="Register"/>
			</form>
		<br>
		<br>
        <div class ="vertline">
			<table>
				<colgroup width=33%; span="3"></colgroup>
				<tr>
					<td>Team 3's Music Festival</td>
					<td>Praktikum Datenbanksysteme SS 16</td>
					<td>Team 3</td>
				</tr>
            </table>   
		</div>
    </body>
</html>