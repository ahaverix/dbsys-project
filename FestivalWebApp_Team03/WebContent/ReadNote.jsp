<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="notes" scope="request" type="java.util.ArrayList<de.tum.in.dbpra.model.bean.NoteInfoBean>"></jsp:useBean>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team 3's Music Festival</title>
        <meta name="Team 3's Music Festival" content="Team 3's Music Festival">
        <link rel="stylesheet" href="./resources/css/custom.css">
    </head>
    <body>
	<%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.html");
		} else
			user = (String) session.getAttribute("user");
		int loginID = -1;
		if (session.getAttribute("loginID") == null) {
			response.sendRedirect("login.html");
		} else {
			 loginID = Integer.valueOf((Integer) session.getAttribute("loginID"));
			 System.out.println(loginID);
			//loginID = Integer.parseInt(s);
		}
		
		String userEmail = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userEmail = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		}
	%>
	<div id="header">
            </div>
        <div class= "bodyDiv">
			<a href="LoginSuccess.jsp">
            	<img id="logo" alt="Logo" src="resources/images/logo.jpg" width=100%/>
			</a>	
        </div>
		<nav>
			<div class ="vertline">
				<table>
					<colgroup width=20%; span="5"></colgroup>
					<tr>
						<td><a class="vertline" href="tickets.jsp">Tickets</a></td>
						<td><a class="vertline" href="timetable.jsp">My Timetable</a></td>
						<td><a class="vertline" href="staff.jsp">Festival Staff</a></td>
						<td></td>
						<td>Logged in: <%=user %> </td>
						<td><form action="LogoutServlet" method="post"><input type="submit" value="Logout" ></form></td>
					</tr>
				</table>   
			</div>
		</nav>
		<h2>Your Notes</h2>
		<% if (request.getAttribute("error") != null) { %>
			<h3 style='color: red;'>No notes could be read!</h3>
			<%= request.getAttribute("error") %>
			<br>
		<% } else{ %>
			In the table below you can see all notes concerning you, ordered by the time when they were posted.
			<br>
			<br>
			<table>
	            <tr>
					<th width="10%">Number</th>
					<th width="28%">Content</th>
					<th width="15%">Posted at</th>
					<th width="15%">Name of Poster</th>
					<th width="15%">Phone of Poster</th>
					<th width="15%">Email of Poster</th>
					<th width="2%"></th>
				</tr>
				<% for (de.tum.in.dbpra.model.bean.NoteInfoBean note : notes){%>
					<tr>
						<td><%= note.getNote_id() %></td>
						<td><%= note.getContent() %></td>
						<td><%= note.getPosted_at() %></td>
						<td><%= note.getOrganizer_name() %></td>
						<td><%= note.getOrganizer_phone() %></td>
						<td><%= note.getOrganizer_email() %></td>
						<td></td>
					</tr>
				<% } %>
			</table>
		<% } %>
		<br>
        <div class ="vertline">
			<table>
				<colgroup width=33%; span="3"></colgroup>
				<tr>
					<td>Team 3's Music Festival</td>
					<td>Praktikum Datenbanksysteme SS 16</td>
					<td>Team 3</td>
				</tr>
            </table>   
		</div>
    </body>
</html>