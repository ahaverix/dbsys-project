<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="personnels" scope="request" type="java.util.ArrayList<de.tum.in.dbpra.model.bean.PersonnelBean>"></jsp:useBean>

<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team 3's Music Festival</title>
        <meta name="Team 3's Music Festival" content="Team 3's Music Festival">
        <link rel="stylesheet" href="./resources/css/custom.css">
    </head>
    <body>
	<%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.html");
		} else
			user = (String) session.getAttribute("user");
		int loginID = -1;
		if (session.getAttribute("loginID") == null) {
			response.sendRedirect("login.html");
		} else {
			 loginID = Integer.valueOf((Integer) session.getAttribute("loginID"));
			 System.out.println(loginID);
			//loginID = Integer.parseInt(s);
		}
		
		String userEmail = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userEmail = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		}
	%>
	<div id="header">
            </div>
        <div class= "bodyDiv">
			<a href="LoginSuccess.jsp">
            	<img id="logo" alt="Logo" src="resources/images/logo.jpg" width=100%/>
			</a>	
        </div>
		<nav>
			<div class ="vertline">
				<table>
					<colgroup width=20%; span="5"></colgroup>
					<tr>
						<td><a class="vertline" href="tickets.jsp">Tickets</a></td>
						<td><a class="vertline" href="timetable.jsp">My Timetable</a></td>
						<td><a class="vertline" href="staff.jsp">Festival Staff</a></td>
						<td></td>
						<td>Logged in: <%=user %> </td>
						<td><form action="LogoutServlet" method="post"><input type="submit" value="Logout" ></form></td>
					</tr>
				</table>   
			</div>
		</nav>
		<h2>Shift Assignment</h2>
		<% if (request.getAttribute("error") != null) { %>
			<h3 style='color: red;'>Shift could not be assigned!</h3>
			<%= request.getAttribute("error") %>
			<br>
			<br>
			<br>
		<% } %>	
		In the table below you can see all personnels that are not allready assigned to shifts that overlap with the one you selected.
		<br>
		<br>
		Please select a personnel from the table or select "Set the shift to unassigned" and finish by clicking on "change assignment" 
		<br>
		<br>
		<form method="post">
			<input type="hidden" name="accessedAfterAssignment" value="true" />
			<input type="hidden" name="shift_id" value=<%=request.getAttribute("shift_id")%> />
			<table>
				<tr>
					<th width="5%"></th>
					<th width="10%">Number</th>
					<th width="10%">Title</th>
					<th width="15%">First Name</th>
					<th width="15%">Last Name</th>
					<th width="15%">Birthday</th>
					<th width="15%">Job</th>
					<th width="15%">Experience</th>
				</tr>
				<% for (de.tum.in.dbpra.model.bean.PersonnelBean personnel : personnels){%>
	            	<tr>
	            		<td><input type="radio" name="personnel_id" value=<%= personnel.getPerson_id() %>></td>
	            		<td><%= personnel.getPerson_id() %></td>
	            		<td><%= personnel.getTitle() %></td>
	            		<td><%= personnel.getFirst_name() %></td>
	            		<td><%= personnel.getLast_name() %></td>
	            		<td><%= personnel.getBirthday() %></td>
	            		<td><%= personnel.getJob() %></td>
	            		<td><%= personnel.getExperience() %></td>
	            	</tr>
            	<% } %>
			</table> 
			<br>
			<table>
				<tr>
            	<td width = 5%><label><input type="radio" name="personnel_id" value=0 checked="checked"></label></td>	
				<td width = 25%>Set the shift to unassigned</td>
				<td width = 70%></td>
				</tr>
			</table>
			<br>
			<br>
			<input type="submit" value="Change Assignment" />
		</form>
		<br>
        <div class ="vertline">
			<table>
				<colgroup width=33%; span="3"></colgroup>
				<tr>
					<td>Team 3's Music Festival</td>
					<td>Praktikum Datenbanksysteme SS 16</td>
					<td>Team 3</td>
				</tr>
            </table>   
		</div>
    </body>
</html>