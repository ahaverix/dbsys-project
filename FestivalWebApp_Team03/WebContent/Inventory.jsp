<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
	import= "de.tum.in.dbpra.model.bean.InventoryShopBean"
    %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team 3's Music Festival</title>
        <meta name="Team 3's Music Festival" content="Team 3's Music Festival">
        <link rel="stylesheet" href="./resources/css/custom.css">
    </head>
    <body>
    <%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.html");
		} else
			user = (String) session.getAttribute("user");
		int loginID = -1;
		if (session.getAttribute("loginID") == null) {
			response.sendRedirect("login.html");
		} else {
			 loginID = Integer.valueOf((Integer) session.getAttribute("loginID"));
			 System.out.println(loginID);
			//loginID = Integer.parseInt(s);
		}
		
		String userEmail = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userEmail = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		}
	%>
        <div id="header">
            </div>
        <div class= "bodyDiv">
			<a href="LoginSuccess.jsp">
            	<img id="logo" alt="Logo" src="./resources/images/logo.jpg" width=100%/>
			</a>	
        </div>
		<nav>
			<div class ="vertline">
				<table>
					<colgroup width=20%; span="5"></colgroup>
					<tr>
						<td><a class="vertline" href="tickets.html">Tickets</a></td>
						<td><a class="vertline" href="timetable.html">My Timetable</a></td>
						<td><a class="vertline" href="staff.jsp">Festival Staff</a></td>
						<td></td>
						<td>Logged in: <%=user %> </td>
						<td><form action="LogoutServlet" method="post"><input type="submit" value="Logout" ></form></td>
					</tr>
				</table>   
			</div>
		<h2>Team 3's Music Festival - The best festival you will ever visit</h2>
		
		<br>
		<br>
		<br>
		<!-- if there is an answer to be displayed -->
		<%if(request.getAttribute("answer")==null){
		}
		else 
			 {%>
		<h3>	<%=request.getAttribute("answer") %></h3>
			<% }%>
		
		<div class="textleft">
		<h3>Update Shop inventory</h3>
		You can either choose to update existing stock in a specific shop, or select a new Product which should be offered in the named shop.
		<br>
		<br>
		<i>Please select the shop in which you would like to update the inventory and select the product. Now you can either add to stock or remove by specifying an amount.
		If you wish to remove from Stock simply type in a negative number.</i>
		<br>
		<br>
		<form action="./Inventory" method="post">
		<table>
		<tr>
		<td>ShopID: <select class="form-control" name="shopID">         
	      			<c:forEach var="shop" items="${shops}">
	          			<option value="${shop.shopID}">${shop.shopName}</option>
	      			</c:forEach>
	   			</select>
		<td>MerchandiseID: <select class="form-control" name="merchandiseID">         
	      			<c:forEach var="merchandise" items="${catalogue}">
	          			<option value="${merchandise.merchandiseID}">${merchandise.merchandiseName}</option>
	      			</c:forEach>
	   			</select>
		<td>Amount added to stock: <input type="text" name="amount" maxlength="9"></td>
		<td><input type="submit"></td></tr>
		<tr></tr>
		<tr></tr>
		</table></form>	
		<br>
		<br>
		</div>
		<br>
		<br>
		<br>
        <div class ="vertline">
			<table>
				<colgroup width=33%; span="3"></colgroup>
				<tr>
					<td>Team 3's Music Festival</td>
					<td>Praktikum Datenbanksysteme SS 16</td>
					<td>Team 3</td>
				</tr>
            </table>   
		</div>
</head>
<body>

</body>
</html>