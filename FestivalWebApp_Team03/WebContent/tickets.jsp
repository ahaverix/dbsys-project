<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team 3's Music Festival</title>
        <meta name="Team 3's Music Festival" content="Team 3's Music Festival">
        <link rel="stylesheet" href="./resources/css/custom.css">
    </head>
    <body>
	<%
		//allow access only if session exists
		String user = null;
		if (session.getAttribute("user") == null) {
			response.sendRedirect("login.html");
		} else
			user = (String) session.getAttribute("user");
		int loginID = -1;
		if (session.getAttribute("loginID") == null) {
			response.sendRedirect("login.html");
		} else {
			 loginID = Integer.valueOf((Integer) session.getAttribute("loginID"));
			 System.out.println(loginID);
			//loginID = Integer.parseInt(s);
		}
		
		String userEmail = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userEmail = cookie.getValue();
				if (cookie.getName().equals("JSESSIONID"))
					sessionID = cookie.getValue();
			}
		}
	%>
	<div id="header">
            </div>
        <div class= "bodyDiv">
			<a href="LoginSuccess.jsp">
            	<img id="logo" alt="Logo" src="resources/images/logo.jpg" width=100%/>
			</a>	
        </div>
		<nav>
			<div class ="vertline">
				<table>
					<colgroup width=20%; span="5"></colgroup>
					<tr>
						<td><a class="vertline" href="tickets.jsp">Tickets</a></td>
						<td><a class="vertline" href="timetable.jsp">My Timetable</a></td>
						<td><a class="vertline" href="staff.jsp">Festival Staff</a></td>
						<td></td>
						<td>Logged in: <%=user %> </td>
						<td><form action="LogoutServlet" method="post"><input type="submit" value="Logout" ></form></td>
					</tr>
				</table>   
			</div>
		</nav>
		<h2>Team 3's Music Festival - The best festival you will ever visit</h2>
		<div class="textleft">
			<h3><a href="booking">Book A Ticket</a></h3>
			Be part of the best festival you will ever visit in your life.
			<br>
			<br>
			You can also buy tickets for your friends, however not more than 20.
			<br>
			<br>
			<a href="booking">Get your ticket now!</a>
		<br><br><br>
			<h3><a href="personalize">Personalize Your Ticket</a></h3>
			Allready have a ticket? Assign it to yourself or to one of your friends by <a href="personalize">personalizing</a> it.
		</div>
		<br>
        <div class ="vertline">
			<table>
				<colgroup width=33%; span="3"></colgroup>
				<tr>
					<td>Team 3's Music Festival</td>
					<td>Praktikum Datenbanksysteme SS 16</td>
					<td>Team 3</td>
				</tr>
            </table>   
		</div>
    </body>
</html>