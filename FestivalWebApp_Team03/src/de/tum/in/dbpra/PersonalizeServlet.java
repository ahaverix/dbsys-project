package de.tum.in.dbpra;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.bean.*;
import de.tum.in.dbpra.model.dao.*;

/**
 * Servlet implementation class PersonalizeServlet
 */
@WebServlet("/personalize")
public class PersonalizeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PersonalizeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/** 
	 *@see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	*/ 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PersonalizeDAO dao = new PersonalizeDAO();
		
		try {
			//get email of the logged in user
			String clientEmail = (String)request.getSession().getAttribute("email");
			
			//get booked tickets
        	ArrayList<TicketBean> tickets;
			tickets = dao.getUnpersonalizedTickets(clientEmail);
			request.setAttribute("tickets", tickets);
			
			//send attributes to jsp
	        request.getRequestDispatcher("/unpersonalized_tickets.jsp").forward(request, response);
	        
		} catch (Throwable e) {
			request.setAttribute("error", e.getMessage());
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/unpersonalized_tickets.jsp");
    		dispatcher.forward(request, response);
		}
       
}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 *//*
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
        	OrderDAO dao = new OrderDAO();
        	OrderBean order = new OrderBean();
        	order.setOrderkey(Integer.parseInt(request.getParameter("orderkey")));
        	dao.getOrdersOK();
        	request.setAttribute("order", order);
    	} catch (Throwable e) {
    		e.printStackTrace();
    		request.setAttribute("error", e.toString() + e.getMessage());
    	}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/exercise72.jsp");
		dispatcher.forward(request, response);
		
	}*/

}

