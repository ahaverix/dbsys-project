package de.tum.in.dbpra;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import de.tum.in.dbpra.model.bean.*;
import de.tum.in.dbpra.model.dao.ReadNoteDAO;

/**
 * Servlet implementation class CustomerServlet
 */

@WebServlet("/ReadNote")
public class ReadNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReadNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /* Gets some information from ReadNoteDAO about all notes regarding the personnel and passes them to ReadNote.jsp.
     * If no such note exists, a corresponding message is shown instead.
     * 
     * The personnel_id is being retrieved from the session.
     * */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			ReadNoteDAO dao = new ReadNoteDAO();
			ArrayList<NoteInfoBean> notes = new ArrayList<NoteInfoBean>();
			//Get personnel_id from session
			int personnel_id = (Integer)request.getSession().getAttribute("loginID");
			dao.getNotesByPersonnelID(personnel_id, notes);
			request.setAttribute("notes", notes);
			if(notes.isEmpty()){
				throw new NoNotesException("There aren't any notes assigned to you yet.");
			}
		}catch (Throwable e) {
    		e.printStackTrace();
    		request.setAttribute("error", e.getMessage());
    	}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/ReadNote.jsp");
		dispatcher.forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	//Exception that is thrown when there aren't any note regarding the personnel
	public static class NoNotesException extends Throwable {
		private static final long serialVersionUID = 1L;
		NoNotesException(String message){
			super(message);
		}
	}
}
