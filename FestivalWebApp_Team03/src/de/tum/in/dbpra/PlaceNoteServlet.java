package de.tum.in.dbpra;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import de.tum.in.dbpra.model.bean.*;
import de.tum.in.dbpra.model.dao.NoteInsertionDAO;
import de.tum.in.dbpra.model.dao.PlaceNoteDAO;

/**
 * Servlet implementation class CustomerServlet
 */

@WebServlet("/PlaceNote")
public class PlaceNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PlaceNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /* Gets a list of all personnels and their shifts from PlaceNoteDAO and forwards it to PlaceNote.jsp.
     * */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			PlaceNoteDAO dao = new PlaceNoteDAO();
			ArrayList<WorkstatusBean> workstatuses = new ArrayList<WorkstatusBean>();
			dao.getWorkstatuses(workstatuses);
			request.setAttribute("workstatuses", workstatuses);
		}catch (Throwable e) {
    		e.printStackTrace();
    		request.setAttribute("error", e.toString() + e.getMessage());
    	}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/PlaceNote.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/* Gets the personnels and content of the note from the request, forwards them to the NoteInsertionDAO for insertion and redirects to NotePlacementSuccessful.jsp.
	 * If no personnels or content were provided, a corresponding error message is displayed.
	 * 
	 * The organizer_id for the note is being retrieved from the session.
	 * */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher;
		try{
			String content = request.getParameter("content");
			if(content.equals("")){
				throw new MissingParameterException("You have to enter a content for your note!");
			}
			if(request.getParameter("personnel_ids")==null){
				throw new MissingParameterException("You have to select personels that should be concerned by your note!");
			}
			String [] personnel_ids = request.getParameterValues("personnel_ids");
			
			//parse String array to int array
			int [] personnel_idsINT = new int [personnel_ids.length];
			for(int i = 0; i < personnel_ids.length; i++){
				personnel_idsINT[i] = Integer.parseInt(personnel_ids[i]);
			}
			
			//Get organizer_id from session
			int organizer_id = (Integer)request.getSession().getAttribute("loginID");
			NoteInsertionDAO dao = new NoteInsertionDAO();
			dao.insertNote(content, organizer_id, personnel_idsINT);
			dispatcher = request.getRequestDispatcher("/NotePlacementSuccessful.jsp");
			dispatcher.forward(request, response);
			//A NullPointerExceotion is catched if no personels were provided
		}catch (Throwable e) {
    		e.printStackTrace();
    		request.setAttribute("error", e.getMessage());
    		doGet(request, response);
    	}
	}
	
	//Exception that is thrown when a parameter is missing
	public static class MissingParameterException extends Throwable {
		private static final long serialVersionUID = 1L;
		MissingParameterException(String message){
			super(message);
		}
	}
}
