package de.tum.in.dbpra;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.bean.ClientBean;
import de.tum.in.dbpra.model.dao.ClientDAO;

/**
 * Servlet implementation class ClientServlet
 */
@WebServlet("/register")
public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/register.jsp");
		dispatcher.forward(request, response);
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException, NumberFormatException {
		try {
        	ClientDAO dao = new ClientDAO();
        	ClientBean client = new ClientBean();
			
			//set attributes 
        	client.setFirstName(request.getParameter("first_name"));
			client.setLastName(request.getParameter("last_name"));
			client.setGender(request.getParameter("gender"));
			client.setTitle(request.getParameter("title"));
			
			String b_day = request.getParameter("b_year") + "-" + request.getParameter("b_month") + "-" + request.getParameter("b_day");
			client.setBirthday(b_day);
			
			client.setPhoneNo(request.getParameter("phone_no"));
			client.setEmail(request.getParameter("email"));
			client.setAddress(request.getParameter("address"));
			client.setPassword(request.getParameter("password"));
			client.setPassword2(request.getParameter("password2"));
			client.setCreditCardNo(request.getParameter("credit_card_no"));
			
			String e_day = request.getParameter("e_year") + "-" + request.getParameter("e_month") + "-" + request.getParameter("e_day");
			client.setExpirationDate(e_day);
			
			if(request.getParameter("is_company") == null){
				client.setIsCompany(-1);
			}
			else{
				client.setIsCompany(Integer.parseInt(request.getParameter("is_company")));
			}
			
			client.setCompanyName(request.getParameter("company_name"));
			
			//insert client
        	dao.InsertClient(client);
        	request.setAttribute("client", client);
        	
        	//send attributes to login page
        	RequestDispatcher dispatcher = request.getRequestDispatcher("RegisterSuccess.html");
    		dispatcher.forward(request, response);
    		
    	} catch (Throwable e) {
    		//e.printStackTrace();
    		request.setAttribute("error", e.getMessage());
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/register.jsp");
    		dispatcher.forward(request, response);
    	}
		
		
	}
	

}
