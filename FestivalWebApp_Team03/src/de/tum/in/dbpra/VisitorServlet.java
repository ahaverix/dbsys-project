package de.tum.in.dbpra;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.bean.*;
import de.tum.in.dbpra.model.dao.*;

/**
 * Servlet implementation class VisitorServlet
 */
@WebServlet("/assignToNew")
public class VisitorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VisitorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/unpersonalized_tickets.jsp");
		dispatcher.forward(request, response);
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
        	PersonalizeDAO dao = new PersonalizeDAO();
        	VisitorBean visitor = new VisitorBean();
        	
        	//get ticket list
        	ArrayList<TicketBean> tickets;
        	
        	//get email of logged in user
        	String clientEmail = (String)request.getSession().getAttribute("email");
        	
        	//get booked tickets
			tickets = dao.getUnpersonalizedTickets(clientEmail);
			request.setAttribute("tickets", tickets);
        
			
			//set attributes
        	visitor.setFirstName(request.getParameter("first_name"));
			visitor.setLastName(request.getParameter("last_name"));
			visitor.setGender(request.getParameter("gender"));
			visitor.setTitle(request.getParameter("title"));
			String b_day = request.getParameter("b_year") + "-" + request.getParameter("b_month") + "-" + request.getParameter("b_day");
			visitor.setBirthday(b_day);
			visitor.setPhoneNo(request.getParameter("phone_no"));
			visitor.setEmail(request.getParameter("email"));
			visitor.setAddress(request.getParameter("address"));
			visitor.setDisability(request.getParameter("disability"));
			
				//default password: birthday; visitor will change it with the first login (feature not implemented yet)
			visitor.setPassword(b_day);
			
			//insert visitor
        	dao.insertVisitor(visitor);
        	request.setAttribute("visitor", visitor);
        	
			
			//send attributes to jsp
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/unpersonalized_tickets.jsp");
    		dispatcher.forward(request, response);
    
			
		} catch (Throwable e) {
    		request.setAttribute("error_visitor", e.getMessage());
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/unpersonalized_tickets.jsp");
    		dispatcher.forward(request, response);
    	}
		
		
	}

}
