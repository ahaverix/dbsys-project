package de.tum.in.dbpra;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.bean.TimetableBean;
import de.tum.in.dbpra.model.dao.TimetableDAO;

/**
 * Servlet implementation class TimetableServlet
 */
@WebServlet({ "/TimetableServlet", "/Timetable" })
public class TimetableServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TimetableServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//make use of the session login ID
			Object value = request.getSession().getAttribute("loginID");
			int loginID = Integer.valueOf((Integer) value);
			
    		TimetableDAO dao = new TimetableDAO();
        	ArrayList<TimetableBean> performances = new ArrayList<TimetableBean>();
			dao.getTable(performances, loginID);
			request.setAttribute("performances", performances);
    	} catch (SQLException e) {
    		e.printStackTrace();
    		request.setAttribute("error", "Couldnt connect to the Database. Please try again later. We are sorry for the inconvenience.");
    	}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Performances.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
