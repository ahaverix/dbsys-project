package de.tum.in.dbpra;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.ShiftServlet.MissingInputException;
import de.tum.in.dbpra.model.bean.BandBean;
import de.tum.in.dbpra.model.dao.BandDAO;
import de.tum.in.dbpra.model.dao.ChoosesDAO;
import de.tum.in.dbpra.model.dao.ShiftInsertionDAO;

/**
 * Servlet implementation class ChooseBandServlet
 */
@SuppressWarnings("unused")
@WebServlet({ "/ChooseBandServlet", "/ChooseBand" })
public class ChooseBandServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
		
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChooseBandServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//make use of the session login ID
			Object value = request.getSession().getAttribute("loginID");
			int loginID = Integer.valueOf((Integer) value);
			System.out.println(loginID); // for debugging
			
    		BandDAO dao = new BandDAO();
        	ArrayList<BandBean> chosenBands = new ArrayList<BandBean>();
        	ArrayList<BandBean> otherBands = new ArrayList<BandBean>();
			dao.getBands(chosenBands, otherBands, loginID);
			request.setAttribute("chosenBands", chosenBands);
        	request.setAttribute("otherBands", otherBands);
    	} catch (SQLException e) {
    		e.printStackTrace();
    		request.setAttribute("error", "Couldnt connect to the Database. Please try again later. We are sorry for the inconvenience.");
    	}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/ChooseBand.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			if(request.getSession().getAttribute("loginID")==null){
				throw new MissingInputException("ERROR: Please Log in! User ID not retrievable...");
			}
			
			//make use of the session login ID
			Object value = request.getSession().getAttribute("loginID");
			int loginID = Integer.valueOf((Integer) value);
			
			//Insert the user-band pairs into chooses
			String[] bands = request.getParameterValues("band_id");
			int[] band_ids = new int[bands.length];
		    int i=0;
		    for(String str:bands){
		    	band_ids[i]=Integer.parseInt(str.trim());
		    	i++;
		    }
			ChoosesDAO dao = new ChoosesDAO();
			dao.insertChooses(loginID, band_ids);
			
			// give feedback when successful
			boolean Reloaded = true;
        	request.setAttribute("Reloaded", Reloaded);
        	doGet(request, response);
		} catch (Throwable e) {
    		e.printStackTrace();
    		request.setAttribute("error", e.getMessage());
    		doGet(request, response);
    	}
	}
	
	//Exception that is thrown when no input was provided
	public static class MissingInputException extends Throwable {		
		private static final long serialVersionUID = 1L;
		MissingInputException(String message){
			super(message);
		}
	}

}
