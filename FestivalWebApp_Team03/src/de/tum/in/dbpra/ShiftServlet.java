package de.tum.in.dbpra;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import de.tum.in.dbpra.model.bean.PersonnelBean;
import de.tum.in.dbpra.model.bean.ShiftBean;
import de.tum.in.dbpra.model.dao.AssignShiftDAO;
import de.tum.in.dbpra.model.dao.ShiftDAO;
import de.tum.in.dbpra.model.dao.ShiftInsertionDAO;

/**
 * Servlet implementation class ShiftServlet
 */
@WebServlet("/Shifts")
public class ShiftServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShiftServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	// Creates two ArrayLists of ShiftBeans(assigned and unassgined), lets the ShiftDAO fill it and passes it to Shifts.jsp.
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
    		ShiftDAO dao = new ShiftDAO();
        	ArrayList<ShiftBean> assignedShifts = new ArrayList<ShiftBean>();
        	ArrayList<ShiftBean> unassignedShifts = new ArrayList<ShiftBean>();
			dao.getShiftsbyAssignment(assignedShifts, unassignedShifts);
			request.setAttribute("assignedShifts", assignedShifts);
        	request.setAttribute("unassignedShifts", unassignedShifts);
        //There should actually never be an Exception in doGet(). If one occurs, we handle it anyways though.	
    	} catch (Throwable e) {
    		e.printStackTrace();
    		request.setAttribute("error", e.getMessage());
    	}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Shifts.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/* The post method of this servlet is called from two different JSPs (AssignShift.jsp and Shifts.jsp) and does two different things respectively.
	 * This is usually not a good idea, but it really makes sense in this case, because both post methods are tightly connected
	 * 	with each other and with the get method (mostly because Exception handling always redirects to the previous page).
	 * Therefore we use two different private doPost methods, doPost_Shifts and doPost_AssignShift.
	 * The doPost method redirects to them depending on the request parameter "accessedAfterAssignment", which is only set in AssignShift.jsp.
	 * */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if ((request.getParameter("accessedAfterAssignment")!=null)&&(request.getParameter("accessedAfterAssignment").equals("true"))){
			doPost_AssignShift(request, response);
		}
		else{
			doPost_Shifts(request, response);
		}
	}
	
	/* Gets the shift_id from the request, forwards it to the AssignShiftDAO, which selects all potential personnels for the shift and forwards those to AssignShift.jsp.
	 * If no shift_id was provided (or an other error occured), the method redirects to the previous page and displays the corresponding error.
	 * */
	private void doPost_Shifts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			if(request.getParameter("shift_id")==null){
				throw new MissingInputException("You have to select a shift from the table that you want to assign!");
			}
			int shift_id = Integer.valueOf(request.getParameter("shift_id"));
    		AssignShiftDAO dao = new AssignShiftDAO();
        	ArrayList<PersonnelBean> personnels = new ArrayList<PersonnelBean>();
			dao.getPersonnelByShiftTimes(personnels, shift_id);
			request.setAttribute("personnels", personnels);
			request.setAttribute("shift_id", shift_id);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/AssignShift.jsp");
			dispatcher.forward(request, response);
    	} catch (Throwable e) {
    		e.printStackTrace();
    		request.setAttribute("error", e.getMessage());
    		doGet(request, response);
    	}
	}
	
	/*Gets shift_id and personnel_id from the request, inserts them into the gets_note table and redirects to the shift page, showing an "insertion successful" message.
	 * If no personnel_id was provided (or an other error occured), the method redirects to the previous page and displays the corresponding error. 
	 * */
	private void doPost_AssignShift(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			//This should actually never occur since we added a default value for the personnel checkbox. We include it anyways, just to be sure.
			if(request.getParameter("personnel_id")==null){
				throw new MissingInputException("You have to select a personnel from the table that you want to assign the shift to!");
			}
			//Insertion
			int shift_id = Integer.valueOf(request.getParameter("shift_id"));
			int personnel_id = Integer.valueOf(request.getParameter("personnel_id"));
			ShiftInsertionDAO dao = new ShiftInsertionDAO();
			dao.assignShift(personnel_id, shift_id);
			//If accessedAfterAssignment is set, an "insertion successful" message is displayed.
			boolean accessedAfterAssignment = true;
        	request.setAttribute("accessedAfterAssignment", accessedAfterAssignment);
        	doGet(request, response);
        //Redirect to the previous page if an error was thrown and display the error message on top of the page	
		} catch (Throwable e) {
    		e.printStackTrace();
    		request.setAttribute("error", e.getMessage());
    		doPost_Shifts(request, response);
    	}
	}
	
	//Exception that is thrown when no input was provided
	public static class MissingInputException extends Throwable {		
		private static final long serialVersionUID = 1L;
		MissingInputException(String message){
			super(message);
		}
	}
}
