package de.tum.in.dbpra;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.dao.*;

@WebServlet(name ="PurchaseServlet", urlPatterns={"/Purchase"})
public class PurchaseServlet extends HttpServlet {
	PurchaseDAO DAO = new PurchaseDAO();	
	String shopID;
	

	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String wristbandID = (String) request.getParameter("wristbandID");
		String[] selectedMerchandiseID = request.getParameterValues("merchandiseID");
		String[] amount = request.getParameterValues("amount");

			try {
				request.setAttribute("shops", DAO.shopList());// Will be available as ${shops} in JSP
				request.setAttribute("shopProducts", DAO.ShopProductList(shopID));// Will be available as ${shopProducts} in JSP
				request.setAttribute("answer", DAO.insertPurchase(shopID, amount, wristbandID, selectedMerchandiseID)); // Will be available as ${answer} in JSP
		        request.getRequestDispatcher("/Purchase.jsp").forward(request, response);
			}  catch (SQLException e) {
	        	e.printStackTrace();
	        	request.setAttribute("answer","Couldnt connect to the Database. Please try again later. We are sorry for the inconvenience.");
	        	request.getRequestDispatcher("/Purchase.jsp").forward(request, response);
	        	
	        }
		}


	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
				shopID = request.getParameter("id");
				if(!(shopID== null)){
					request.setAttribute("id", shopID); // Will be available as ${id} in JSP
					request.setAttribute("shopProducts", DAO.ShopProductList(shopID));
				}
            	request.setAttribute("shops", DAO.shopList());	// Will be available as ${shops} in JSP
                request.getRequestDispatcher("/Purchase.jsp").forward(request, response);
        
		} catch (SQLException e) {
        	e.printStackTrace();
        	request.setAttribute("answer","Couldnt connect to the Database. Please try again later. We are sorry for the inconvenience.");
        	request.getRequestDispatcher("/Purchase.jsp").forward(request, response);
        	
        }
	}
}