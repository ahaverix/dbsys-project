package de.tum.in.dbpra;

import java.io.IOException;
import java.sql.SQLException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.dao.*;

@WebServlet(name ="InventoryServlet", urlPatterns={"/Inventory"})
public class InventoryServlet extends HttpServlet {
	
	

	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
        	InventoryDAO DAO = new InventoryDAO();
            request.setAttribute("shops", DAO.shopList()); // Will be available as ${shops} in JSP
            request.setAttribute("catalogue", DAO.merchandiseList()); // Will be available as ${shops} in JSP
            request.getRequestDispatcher("/Inventory.jsp").forward(request, response);

		  
        } catch (SQLException e) {
        	e.printStackTrace();
        	request.setAttribute("answer","Couldnt connect to the Database. Please try again later. We are sorry for the inconvenience");
        	request.getRequestDispatcher("/Inventory.jsp").forward(request, response);
        	
        }
    }
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String shopID, merchandiseID, amount;
		
		shopID = request.getParameter("shopID");
		merchandiseID = request.getParameter("merchandiseID");
		amount = request.getParameter("amount");
		//to escape NullPointers.
		if (amount == null || amount.equals(""))
			amount = "a";		
		
		try {
			InventoryDAO DAO = new InventoryDAO();
			request.setAttribute("answer", DAO.insertUpdate(shopID, merchandiseID, amount));
			request.setAttribute("shops", DAO.shopList()); // Will be available as ${shops} in JSP
            request.setAttribute("catalogue", DAO.merchandiseList()); // Will be available as ${shops} in JSP
            request.getRequestDispatcher("/Inventory.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
			request.setAttribute("answer","Couldnt connect to the Database. Please try again later. We are sorry for the inconvenience");
			request.getRequestDispatcher("/Inventory.jsp").forward(request, response);
			
		}
	}
}