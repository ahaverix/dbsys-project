package de.tum.in.dbpra.model.bean;

public class InventoryUpdateBean {
	private int shopID;
	private int merchandiseID;
	private int amount;
	private String merchandiseName;
	private String shopName;
	
	public int getShopID() {
		return shopID;
	}
	public String getMerchandiseName() {
		return merchandiseName;
	}
	public void setMerchandiseName(String merchandiseName) {
		this.merchandiseName = merchandiseName;
	}
	public void setShopID(int shopID) {
		this.shopID = shopID;
	}
	public int getMerchandiseID() {
		return merchandiseID;
	}
	public void setMerchandiseID(int merchandiseID) {
		this.merchandiseID = merchandiseID;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
}
