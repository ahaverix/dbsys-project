package de.tum.in.dbpra.model.bean;

import java.sql.Timestamp;

public class TimetableBean {
	private Timestamp startPlay;
	private Timestamp endPlay;
	private String bandName;
	private String location;
	private Number latitude;
	private Number longitude;

	public TimetableBean(){}
	
	public Timestamp getStartPlay() {
		return startPlay;
	}
	
	public void setStartPlay(Timestamp startPlay) {
		this.startPlay = startPlay;
	}
	
	public Timestamp getEndPlay() {
		return endPlay;
	}
	
	public void setEndPlay(Timestamp endPlay) {
		this.endPlay = endPlay;
	}
	
	public String getBandName() {
		return bandName;
	}
	
	public void setBandName(String bandName) {
		this.bandName = bandName;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

	public Number getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Number latitude) {
		this.latitude = latitude;
	}
	
	public Number getLongitude() {
		return longitude;
	}
	
	public void setLongitude(Number longitude) {
		this.longitude = longitude;
	}
}
