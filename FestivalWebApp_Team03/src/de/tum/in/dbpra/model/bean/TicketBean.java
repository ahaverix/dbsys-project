package de.tum.in.dbpra.model.bean;

import java.math.BigDecimal;

public class TicketBean {
	private int ticket_id;
	private String type;
	private String valid_from;
	private String area;
	private int vip;
	private int client_id;
	private int visitor_id;
	private BigDecimal price;
	private String booking_time;
	
	public TicketBean() {}
	
	public int getTicketID() {
		return ticket_id;
	}
	public void setTicketID(int ticket_id) {
		this.ticket_id = ticket_id;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getValidFrom() {
		return valid_from;
	}
	public void setValidFrom() {
		if(this.type.equals("friday") || this.type.equals("weekend")){
			this.valid_from = "2016-07-22";
		}
		else if (this.type.equals("saturday")){
			this.valid_from = "2016-07-23";
		}
		else{
			this.valid_from = "2016-07-24";
		}
	}
	
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
	public int getVip() {
		return vip;
	}
	public void setVip(int vip) {
		this.vip = vip;
	}
	
	
	public int getClientID() {
		return client_id;
	}
	public void setClientID(int client_id) {
		this.client_id = client_id;
	}
	
	
	public int getVisitorID() {
		return visitor_id;
	}
	public void setVisitorID(int visitor_id) {
		this.visitor_id = visitor_id;
	}
	
	
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice() {
		if(this.vip == 0){
			if(this.area.equals("concert")&& !this.type.equals("weekend")){
				this.price =  new BigDecimal(50.00);
			}
			else if(this.area.equals("camping") && !this.type.equals("weekend")){
				this.price = new BigDecimal(30.00);
			}
			else if(this.area.equals("both") && !this.type.equals("weekend")){
				this.price = new BigDecimal(80.00);
			}
			else if(this.area.equals("concert") && this.type.equals("weekend")){
				this.price = new BigDecimal(140.00);
			}
			else if(this.area.equals("camping") && this.type.equals("weekend")){
				this.price = new BigDecimal(80.00);
			}
			else if(this.area.equals("both")&& this.type.equals("weekend")){
				this.price = new BigDecimal(220.00);
			}	
		}
		else{
			if(this.area.equals("concert") && !this.type.equals("weekend")){
				this.price = new BigDecimal(80.00);
			}
			else if(this.area.equals("camping") && !this.type.equals("weekend")){
				this.price = new BigDecimal(60.00);
			}
			else if(this.area.equals("both") && !this.type.equals("weekend")){
				this.price = new BigDecimal(110.00);
			}
			else if(this.area.equals("concert") && this.type.equals("weekend")){
				this.price = new BigDecimal(170.00);
			}
			else if(this.area.equals("camping") && this.type.equals("weekend")){
				this.price = new BigDecimal(110.00);
			}
			else if(this.area.equals("both") && this.type.equals("weekend")){
				this.price = new BigDecimal(250.00);
			}
		}
	}
	
	public String getBookingTime() {
		return booking_time;
	}
	public void setBookingTime(String booking_time) {
		this.booking_time = booking_time;
	}
	
}
