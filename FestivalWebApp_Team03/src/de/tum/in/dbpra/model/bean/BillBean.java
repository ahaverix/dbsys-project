package de.tum.in.dbpra.model.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class BillBean{
		private String merchandiseName;
		private String billing;
		private BigDecimal merchandisePrice;
		private int amount;
		private Timestamp time;
		private BigDecimal totalPrice;
		
		public BigDecimal getTotalPrice() {
			return totalPrice;
		}
		public void setTotalPrice(BigDecimal merchandisePrice, int amount) {
			this.totalPrice = merchandisePrice.multiply(new BigDecimal(amount));
		}
		public String getMerchandiseName() {
			return merchandiseName;
		}
		public void setMerchandiseName(String merchandiseName) {
			this.merchandiseName = merchandiseName;
		}
		public BigDecimal getMerchandisePrice() {
			return merchandisePrice;
		}
		public void setMerchandisePrice(BigDecimal merchandisePrice) {
			this.merchandisePrice = merchandisePrice;
		}
		public int getAmount() {
			return amount;
		}
		public void setAmount(int amount) {
			this.amount = amount;
		}
		public Timestamp getTime() {
			return time;
		}
		public void setTime(Timestamp time) {
			this.time = time;
		}
		public String getBilling() {
			return billing;
		}
		public void setBilling(String billing) {
			this.billing = billing;
		}
}
