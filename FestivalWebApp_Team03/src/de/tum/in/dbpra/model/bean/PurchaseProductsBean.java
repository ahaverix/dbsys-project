package de.tum.in.dbpra.model.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class PurchaseProductsBean {
	private String merchandiseName;
	private BigDecimal merchandisePrice;
	private int merchandiseID;
	private int amount;
	private Timestamp time;
	
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Timestamp getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
		this.time = time;
	}
	public String getMerchandiseName() {
		return merchandiseName;
	}
	public void setMerchandiseName(String merchandiseName) {
		this.merchandiseName = merchandiseName;
	}
	public BigDecimal getMerchandisePrice() {
		return merchandisePrice;
	}
	public void setMerchandisePrice(BigDecimal merchandisePrice) {
		this.merchandisePrice = merchandisePrice;
	}
	public int getMerchandiseID() {
		return merchandiseID;
	}
	public void setMerchandiseID(int merchandiseID) {
		this.merchandiseID = merchandiseID;
	}

	
}
