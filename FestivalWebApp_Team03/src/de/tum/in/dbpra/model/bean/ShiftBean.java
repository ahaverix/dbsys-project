package de.tum.in.dbpra.model.bean;

import java.sql.Timestamp;

public class ShiftBean {
	private int shift_id;
	private Timestamp start_time;
	private Timestamp end_time;
	private String task;
	//location is the type of the location where the shift is performed
	private String location;
	//assigned_to is the first and last name of the corresponding personnel entry
	private String assigned_to;
	
	public ShiftBean(){}
	
	public int getShift_id() {
		return shift_id;
	}
	
	public void setShift_id(int shift_id) {
		this.shift_id = shift_id;
	}
	
	public Timestamp getStart_time() {
		return start_time;
	}
	
	public void setStart_time(Timestamp start_time) {
		this.start_time = start_time;
	}
	
	public Timestamp getEnd_time() {
		return end_time;
	}
	
	public void setEnd_time(Timestamp end_time) {
		this.end_time = end_time;
	}
	
	public String getTask() {
		return task;
	}
	
	public void setTask(String task) {
		this.task = task;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

	public String getAssigned_to() {
		return assigned_to;
	}

	public void setAssigned_to(String assigned_to) {
		this.assigned_to = assigned_to;
	}
	
}
