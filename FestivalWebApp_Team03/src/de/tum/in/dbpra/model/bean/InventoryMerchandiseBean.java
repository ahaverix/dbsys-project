package de.tum.in.dbpra.model.bean;

import java.math.BigDecimal;

public class InventoryMerchandiseBean {
	private int merchandiseID;
	private String merchandiseName;
	private BigDecimal price;
	
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal d) {
		this.price = d;
	}
	public int getMerchandiseID() {
		return merchandiseID;
	}
	public void setMerchandiseID(int merchandiseID) {
		this.merchandiseID = merchandiseID;
	}
	public String getMerchandiseName() {
		return merchandiseName;
	}
	public void setMerchandiseName(String merchandiseName) {
		this.merchandiseName = merchandiseName;
	}
	
	
}
