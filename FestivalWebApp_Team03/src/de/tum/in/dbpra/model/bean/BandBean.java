package de.tum.in.dbpra.model.bean;

public class BandBean {
	private int bandId;
	private String bandName;
	
	public BandBean(){}
	
	public int getBandId() {
		return bandId;
	}
	
	public void setBandId(int bandId) {
		this.bandId = bandId;
	}
	
	public String getBandName() {
		return bandName;
	}
	
	public void setBandName(String bandName) {
		this.bandName = bandName;
	}
}
