package de.tum.in.dbpra.model.bean;

import java.sql.Timestamp;

public class WorkstatusBean {
	private int personnel_id;
	private String personnel_first_name;
	private String personnel_last_name;
	private String personnel_job;
	private String personnel_experience;
	private Timestamp shift_start_time;
	private Timestamp shift_end_time;
	private String shift_task;
	//location is the type of the corresponding location
	private String shift_location;
	
	public WorkstatusBean(){}

	public int getPersonnel_id() {
		return personnel_id;
	}

	public void setPersonnel_id(int personnel_id) {
		this.personnel_id = personnel_id;
	}

	public String getPersonnel_first_name() {
		return personnel_first_name;
	}

	public void setPersonnel_first_name(String personnel_first_name) {
		this.personnel_first_name = personnel_first_name;
	}

	public String getPersonnel_last_name() {
		return personnel_last_name;
	}

	public void setPersonnel_last_name(String personnel_last_name) {
		this.personnel_last_name = personnel_last_name;
	}

	public String getPersonnel_job() {
		return personnel_job;
	}

	public void setPersonnel_job(String personnel_job) {
		this.personnel_job = personnel_job;
	}

	public String getPersonnel_experience() {
		return personnel_experience;
	}

	public void setPersonnel_experience(String personnel_experience) {
		this.personnel_experience = personnel_experience;
	}

	public Timestamp getShift_start_time() {
		return shift_start_time;
	}

	public void setShift_start_time(Timestamp shift_start_time) {
		this.shift_start_time = shift_start_time;
	}

	public Timestamp getShift_end_time() {
		return shift_end_time;
	}

	public void setShift_end_time(Timestamp shift_end_time) {
		this.shift_end_time = shift_end_time;
	}

	public String getShift_task() {
		return shift_task;
	}

	public void setShift_task(String shift_task) {
		this.shift_task = shift_task;
	}

	public String getShift_location() {
		return shift_location;
	}

	public void setShift_location(String shift_location) {
		this.shift_location = shift_location;
	}
}
