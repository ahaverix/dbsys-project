package de.tum.in.dbpra.model.bean;

public class VisitorBean {
	private int person_id;
	private String first_name;
	private String last_name;
	private String gender;
	private String title;
	private String birthday;
	private String phone_no;
	private String email;
	private String address;
	private String password;
	
	private String disability;

	
	public VisitorBean() {}
	
	public int getPersonId() {
		return person_id;
	}
	public void setPersonId(int person_id) {
		this.person_id = person_id;
	}
	
	
	public String getFirstName() {
		return first_name;
	}
	
	public void setFirstName(String first_name) {
		this.first_name = first_name;
	}
	
	
	public String getLastName() {
		return last_name;
	}
	
	public void setLastName(String last_name) {
		this.last_name = last_name;
	}
	
	
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getBirthday() {
		return birthday;
	}
	
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	
	
	
	public String getPhoneNo() {
		return phone_no;
	}
	
	public void setPhoneNo(String phone_no) {
		this.phone_no = phone_no;
	}
	

	
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	
	public String getDisability() {
		return disability;
	}
	
	public void setDisability(String disability) {
		this.disability = disability;
	}
	
}
