package de.tum.in.dbpra.model.bean;

import java.sql.Timestamp;

public class NoteInfoBean {
	private int note_id;
	private String content;
	private Timestamp posted_at;
	private String organizer_name;
	private String organizer_phone;
	private String organizer_email;
	
	public NoteInfoBean(){}

	public int getNote_id() {
		return note_id;
	}

	public void setNote_id(int note_id) {
		this.note_id = note_id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getPosted_at() {
		return posted_at;
	}

	public void setPosted_at(Timestamp posted_at) {
		this.posted_at = posted_at;
	}

	public String getOrganizer_name() {
		return organizer_name;
	}

	public void setOrganizer_name(String organizer_name) {
		this.organizer_name = organizer_name;
	}

	public String getOrganizer_phone() {
		return organizer_phone;
	}

	public void setOrganizer_phone(String organizer_phone) {
		this.organizer_phone = organizer_phone;
	}

	public String getOrganizer_email() {
		return organizer_email;
	}

	public void setOrganizer_email(String organizer_email) {
		this.organizer_email = organizer_email;
	}
}
