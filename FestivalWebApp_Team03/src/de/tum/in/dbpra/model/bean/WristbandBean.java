package de.tum.in.dbpra.model.bean;

import java.math.BigDecimal;

public class WristbandBean {
 
	private int wristbandID;
	private BigDecimal currentBalance;
	private String visitorFirstName;
	private String visitorLastName;
	private String fullName;
	
	
	public int getWristbandID() {
		return wristbandID;
	}
	public void setWristbandID(int wristbandID) {
		this.wristbandID = wristbandID;
	}
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}
	public String getVisitorFirstName() {
		return visitorFirstName;
	}
	public void setVisitorFirstName(String visitorFirstName) {
		this.visitorFirstName = visitorFirstName;
	}
	public String getVisitorLastName() {
		return visitorLastName;
	}
	public void setVisitorLastName(String visitorLastName) {
		this.visitorLastName = visitorLastName;
	}
	
	public void setFullName(String rst, String lst){
		this.fullName = rst + " " + lst;
	}
	public String getFullName() {
		return fullName;
	}
 
}
