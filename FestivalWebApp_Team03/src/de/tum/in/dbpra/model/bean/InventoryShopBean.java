package de.tum.in.dbpra.model.bean;

public class InventoryShopBean {
	
	private int shopID;
	private String shopName;
	
	public int getShopID() {
		return shopID;
	}
	public void setShopID(int shopID) {
		this.shopID = shopID;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
}