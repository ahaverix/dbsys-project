package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.tum.in.dbpra.model.bean.WorkstatusBean;

public class PlaceNoteDAO extends AbstractDAO {
	public void getWorkstatuses(ArrayList<WorkstatusBean> workstatuses) throws SQLException{
		//Get information about all personnels and their shifts
		String query = "SELECT p.person_id, p.first_name, p.last_name, pl.job, pl.experience, s.start_time, s.end_time, s.task, l.type AS location " +
				"FROM (person p JOIN personnel pl ON p.person_id = pl.person_id) " +
				"LEFT OUTER JOIN (shift s JOIN location l ON l.location_id = s.executed_in) ON s.assigned_to = p.person_id " +
				"ORDER BY person_id;";
		
		//Gets connection and starts transaction 
		Connection con = getConnection();
		con.setAutoCommit(false);
		
		//Prepare and execute the statement 
		PreparedStatement pstmt = con.prepareStatement(query);
		ResultSet rs = pstmt.executeQuery();
		
		//Add a WorkstatusBean for each entry in the ResultSet to workstatuses
		while(rs.next()) {
			WorkstatusBean workstatus = new WorkstatusBean();
			workstatus.setPersonnel_id(rs.getInt("person_id"));
			workstatus.setPersonnel_first_name(rs.getString("first_name"));
			workstatus.setPersonnel_last_name(rs.getString("last_name"));
			workstatus.setPersonnel_job(rs.getString("job"));
			workstatus.setPersonnel_experience(rs.getString("experience"));
			workstatus.setShift_start_time(rs.getTimestamp("start_time"));
			workstatus.setShift_end_time(rs.getTimestamp("end_time"));
			workstatus.setShift_task(rs.getString("task"));
			workstatus.setShift_location(rs.getString("location"));
			workstatuses.add(workstatus);
		}
		
		//Commit and close everything
		rs.close();
		pstmt.close();
		con.commit();
		con.close();
	}
}
