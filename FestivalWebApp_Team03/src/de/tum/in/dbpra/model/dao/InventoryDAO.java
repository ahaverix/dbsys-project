package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import de.tum.in.dbpra.model.bean.InventoryMerchandiseBean;
import de.tum.in.dbpra.model.bean.InventoryShopBean;
import de.tum.in.dbpra.model.bean.InventoryUpdateBean;

public class InventoryDAO extends AbstractDAO {
	
	/*
	 * HINT: read also the //System.out.println(); They have been used for debugging and are very helpful in understanding the code. 
	 * HINT: Helping methods are shown below.
	 */
	// a simple method which checks wether a String is an Integer
		public boolean isStringInt(String s)
		{
		    try
		    {
		        Integer.parseInt(s);
		        return true;
		    } catch (NumberFormatException ex)
		    {
		        return false;
		    }
		}
//		Generates the list of shops
	    public List<InventoryShopBean> shopList() throws SQLException {
	        List<InventoryShopBean> shops = new ArrayList<InventoryShopBean>();

	        try (
	            Connection connection = getConnection()) {
        		connection.setAutoCommit(false);
	            PreparedStatement statement = connection.prepareStatement("SELECT shop_id, shop_name FROM shop");
	            ResultSet resultSet = statement.executeQuery();
	            while (resultSet.next()) {
	                InventoryShopBean shop = new InventoryShopBean();
	                shop.setShopID(resultSet.getInt("shop_id"));
	                shop.setShopName(resultSet.getString("shop_name"));
	                shops.add(shop);
	            }
	       resultSet.close();
	       connection.commit();
	       connection.close();    
	       statement.close();
	        }
//	        System.out.println("Data Fetched");
	        return shops;
	    }
	    // generates the merchandise list
	    public List<InventoryMerchandiseBean> merchandiseList() throws SQLException {
	        List<InventoryMerchandiseBean> catalogue = new ArrayList<InventoryMerchandiseBean>();

	        try (   Connection connection = getConnection()){
	        	connection.setAutoCommit(false);
		      	PreparedStatement statement = connection.prepareStatement("SELECT merchandise_id, name, price::money::numeric FROM merchandise");
			    ResultSet resultSet = statement.executeQuery();	
	            while (resultSet.next()) {
	                InventoryMerchandiseBean merchandise = new InventoryMerchandiseBean();
	                merchandise.setMerchandiseID(resultSet.getInt("merchandise_id"));
	                merchandise.setMerchandiseName(resultSet.getString("name"));
	                merchandise.setPrice(resultSet.getBigDecimal("price"));
	                catalogue.add(merchandise);
	            }
	            resultSet.close();
	            connection.commit();
	            connection.close();
	            statement.close();
	        }
//	        System.out.println("Data Fetched");
	        
	        return catalogue;
	    }
	    //The insert Update function.
	    public String insertUpdate(String shopID, String merchandiseID, String amount ) throws SQLException{
	    	InventoryUpdateBean added = new InventoryUpdateBean();
	    	String response = null;
	    	 if(!isStringInt(amount)){
	    		 response = "You have entered an invalid amount. Please retry inserting a new value. Only positive and negative numbers are allowed.";
	    	 }
	    	 else {
//	    		 System.out.println("Input was correct.");
	    		 added.setShopID(Integer.parseInt(shopID));
	    		 added.setMerchandiseID(Integer.parseInt(merchandiseID));
	    		 added.setAmount(Integer.parseInt(amount));
	 	    	 boolean checkedAmount = false; // check if there is enough merchandise left, see below 
	 	    	 
	 	    	 try (
			            Connection connection = getConnection();
			           
			        ) {
	 	    		 	connection.setAutoCommit(false);
	 	    	
		    			PreparedStatement amountStatement = connection.prepareStatement("select exists(select 1 from merchandise where merchandise_id=? and quantity>?)");
		  
		    			amountStatement.setInt(1, added.getMerchandiseID());
		    			amountStatement.setInt(2, added.getAmount());
		    			
		    			ResultSet amountResult = amountStatement.executeQuery();
		    			
		    			while (amountResult.next()){
		    				checkedAmount = amountResult.getBoolean(1);
		    			}

//		    			System.out.println("Created the statement checking the result.");

		    			amountResult.close();
		    			amountStatement.close();
		   	    	 if(!checkedAmount){
		   	    		 response = "There is not enough left in the Festival stock. Please specify a smaller number.";
		   	    	 }
		   	    	 else {//System.out.println("Correct values again.. now will check wether inventory eintrag exists");
		   	    		 
		   	    		 //Add the Merchandise Name.
			    		 PreparedStatement nameStatement = connection.prepareStatement("SELECT name FROM merchandise WHERE merchandise_id = ?");
			    		 nameStatement.setInt(1,added.getMerchandiseID());
			    		 ResultSet nameResult = nameStatement.executeQuery();
			    		 
			    		 while(nameResult.next()){
			    			 added.setMerchandiseName(nameResult.getString(1));
			    		 }
			    		 nameStatement.close();
			    		 nameResult.close();
			    		 
			    		//Add the Shop Name.
			    		 PreparedStatement shopStatement = connection.prepareStatement("SELECT shop_name FROM shop WHERE shop_id = ?");
			    		 shopStatement.setInt(1,added.getShopID());
			    		 ResultSet shopResult = shopStatement.executeQuery();
			    		 
			    		 while(shopResult.next()){
			    			 added.setShopName(shopResult.getString(1));
			    		 }
			    		 nameStatement.close();
			    		 nameResult.close();
			    		 
			    		 //Here we check wether inventory eintrag exists. 
		   	    		 boolean checkedInventory = false;
		   	    		 PreparedStatement inventoryStatement = connection.prepareStatement("select exists(select 1 from inventory where shop_id=? and merchandise_id=?)");
		   	    		 
		   	    		 inventoryStatement.setInt(1, added.getShopID());
		   	    		 inventoryStatement.setInt(2, added.getMerchandiseID());
		   	    		 
		   	    		 ResultSet inventoryResult = inventoryStatement.executeQuery();
		   	    		 
		   	    		 while(inventoryResult.next()){
		   	    			 checkedInventory = inventoryResult.getBoolean(1);
		   	    		 }
		   	    		 inventoryResult.close();
		   	    		 inventoryStatement.close();
		   	    		 
		   	    		 if(checkedInventory){
//		   	    			 System.out.println("Eintrag exists so now we check if sum was minus and reduces total amount to -x");
			   	    			try {
			   	    				PreparedStatement updateStatement = connection.prepareStatement("UPDATE inventory SET amount = amount + ? WHERE shop_id = ? AND merchandise_id = ?;");
				   	    			 updateStatement.setInt(1, added.getAmount());
				   	    			 updateStatement.setInt(2, added.getShopID());
				   	    			 updateStatement.setInt(3, added.getMerchandiseID());
				   	    			 updateStatement.executeUpdate();
				   	    			 connection.commit();
				   	    			response = "Storage quantitiy of \""+added.getMerchandiseName()+"\" has been updated by " +added.getAmount() +" in: \""+ added.getShopName()+"\".";
				   	    			updateStatement.close();
				   	    			connection.close();
								} catch (Exception nacaukarin) {
									response = "You cannot remove that many \""+added.getMerchandiseName()+"\" from \""+added.getShopName() +"\". There are not enough in Stock. Please try again using a different amount.";	
								}


		   	    		 }
		   	    		 else{
//	   	    				 System.out.println("No inventory eintrag what about if it is minus?");
		   	    			 if(added.getAmount()<0){
		   	    				 response = "There is no \""+added.getMerchandiseName()+"\" in stock in \""+added.getShopName()+"\". Therefore you cannot remove any from the Inventory. Please try again using a different amount";
		   	    			 }
		   	    			 else{
//		   	    				 System.out.println("No its not so go ahead.. insert");
		   	    				 PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO inventory values (?,?,?)");
		   	    				 insertStatement.setInt(1, added.getShopID());
		   	    				 insertStatement.setInt(2, added.getMerchandiseID());
		   	    				 insertStatement.setInt(3, added.getAmount());
		   	    				 insertStatement.executeUpdate();
		   	    				 connection.commit();
		   	    				 response = "\""+added.getMerchandiseName()+"\" has been added for the first time to \""+added.getShopName()+"\". The amount in stock is now: " + added.getAmount();
		   	    				 insertStatement.close();
		   	    				 connection.close();
		   	    			 }
		   	    		 }
		   	    	 	
		   	    	 }
		    		
		    	} catch (SQLException e) {
					System.out.println("Couldnt connect to database.");
					e.printStackTrace();
					response ="Couldnt connect to Database, Please try again.";
				}
	    		 
	    	 }
//	    	System.out.println("Response: " + response); 
	    	return response;
	    	
	    }
//		TESTING	    
//	    public static void main(String[] args) throws SQLException {
//			InventoryDAO  DAO= new InventoryDAO();
//			String shopID = "1";
//			String merchandiseID = "1";
//			String amount = "-4";
//			
//			DAO.insertUpdate(shopID, merchandiseID, amount);
//			
//		}
}
