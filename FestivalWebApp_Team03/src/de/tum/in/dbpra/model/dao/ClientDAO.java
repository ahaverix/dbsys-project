package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.sun.xml.internal.ws.util.StringUtils;


import de.tum.in.dbpra.model.bean.ClientBean;

public class ClientDAO extends AbstractDAO{
	public void InsertClient(ClientBean client) throws  InvalidInputException, SQLException, ClassNotFoundException {
		
		//check that all not-null fields are provided
		if(client.getFirstName().isEmpty() || client.getLastName().isEmpty()  || client.getGender() == null ||
		   client.getBirthday().isEmpty()  || client.getEmail().isEmpty()  || client.getPassword().isEmpty() || client.getPassword2().isEmpty() ||
		   client.getCreditCardNo().isEmpty() || client.getExpirationDate().isEmpty() || client.getIsCompany()== -1){
			   throw new InvalidInputException("The following fields cannot be empty: first name, last name, gender," +
			   		" date of birth, credit card number, expiration date, company,  email, password, confirmed password.");
		}
		
		//check date is set
		if(client.getBirthday().contains("s")){
			throw new InvalidInputException("Date of birth not set. Pleas set your date of birth.");
		}
			
		//check birthday is valid
		int b_year = Integer.parseInt(client.getBirthday().substring(0, 4));
		if((client.getBirthday().substring(5, 7).equals("02") && client.getBirthday().substring(8, 10).equals("31")) ||
				(client.getBirthday().substring(5, 7).equals("02") && client.getBirthday().substring(8, 10).equals("30")) ||	
				(client.getBirthday().substring(5, 7).equals("04") && client.getBirthday().substring(8, 10).equals("31")) ||	
				(client.getBirthday().substring(5, 7).equals("06") && client.getBirthday().substring(8, 10).equals("31")) ||
				(client.getBirthday().substring(5, 7).equals("09") && client.getBirthday().substring(8, 10).equals("31")) ||
				(client.getBirthday().substring(5, 7).equals("11") && client.getBirthday().substring(8, 10).equals("31")) 
				){
			throw new InvalidInputException("Date " + client.getBirthday() + " is invalid.");
		}
		else if(b_year%4 !=0 && client.getBirthday().substring(8, 10).equals("29") && client.getBirthday().substring(5, 7).equals("02")){
			throw new InvalidInputException("Date " + client.getBirthday() + " is invalid.");		
		}
	
		//check phone number consists only of numbers
		if (!(client.getPhoneNo().matches("[0-9]+") || client.getPhoneNo().isEmpty())){
			throw new InvalidInputException("The phone number can contain only numbers (no letters and other signs).");
		}
		
		//check for right e-mail format	
		String email =  client.getEmail();
		int ct =   email.length() - email.replace("@", "").length();
		if(ct != 1 || !client.getEmail().contains(".")){
			throw new InvalidInputException("Wrong e-mail format. The e-mail has to be of the following form: \'example@abc.xy\'. ");
		}
		
		//check for right credit card number format
		if (!client.getCreditCardNo().matches("[0-9]+") || client.getCreditCardNo().length() != 16) {
			throw new InvalidInputException("Wrong credit card number! The credit card number is 16-digit-long and contains only numbers.");
		}
		
		//check expiration date is set
				if(client.getExpirationDate().contains("s")){
					throw new InvalidInputException("Expiration date not set.");
				}
		
		//check expiration date is valid date
		int e_year = Integer.parseInt(client.getExpirationDate().substring(0, 4));
		if((client.getExpirationDate().substring(5, 7).equals("02") && client.getExpirationDate().substring(8, 10).equals("31")) ||
				(client.getExpirationDate().substring(5, 7).equals("02") && client.getExpirationDate().substring(8, 10).equals("30")) ||	
				(client.getExpirationDate().substring(5, 7).equals("04") && client.getExpirationDate().substring(8, 10).equals("31")) ||	
				(client.getExpirationDate().substring(5, 7).equals("06") && client.getExpirationDate().substring(8, 10).equals("31")) ||
				(client.getExpirationDate().substring(5, 7).equals("09") && client.getExpirationDate().substring(8, 10).equals("31")) ||
				(client.getExpirationDate().substring(5, 7).equals("11") && client.getExpirationDate().substring(8, 10).equals("31")) 
				){
			throw new InvalidInputException("Date " + client.getExpirationDate() + " is invalid.");
		}
		else if(e_year%4 !=0 && client.getExpirationDate().substring(8, 10).equals("29") && client.getExpirationDate().substring(5, 7).equals("02")){
			throw new InvalidInputException("Date " + client.getExpirationDate() + " is invalid.");		
		}
		
		//check that credit card is not expired
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		cal.add(Calendar.DATE, -1);
		cal.clear(Calendar.HOUR); cal.clear(Calendar.MINUTE); cal.clear(Calendar.SECOND);
		Date today = cal.getTime();
		Date exp_date = cal.getTime();;
		try {
			 exp_date = dateFormat.parse(client.getExpirationDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(today.after(exp_date)){
			throw new InvalidInputException("Your credit card is expired.");
		}
		
				
		//check that company name is provided for iscompany = 'y'
		if(client.getIsCompany() == 1 && client.getCompanyName().isEmpty()){
			throw new InvalidInputException("You set the is-company-flag to \'yes\'. Please fill in the company name.");
		}
		
		//check that company name is not provided for iscompany = 'n'
				if(client.getIsCompany() == 0 && !client.getCompanyName().isEmpty()){
					throw new InvalidInputException("You set the is-company-flag to \'no\', but you also provided a company name. This is not allowed.");
				}
		
		
				
		//check that the 2 passwords match
		if(!client.getPassword().equals(client.getPassword2())){
			throw new InvalidInputException("The passwords do not match.");
		}
		
		//password between 6 and 20 characters
		if(client.getPassword().length() < 6 || client.getPassword().length() >20){
			throw new InvalidInputException("The password must contain between 6 and 20 characters.");
		}
		
		
		
		
			
		//db queries
		String query_maxPersonID = "select max(person_id) from person;";
		String query_person = "insert into person values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		String query_client = "insert into client values (?, ?, ?, ?, ?)";
		String query_email = "select * from person where email = ?;"; //for assertion that the email is not used
		
		Connection con = getConnection();
		
		//prepared statements
		PreparedStatement pstmt_maxPersonID = con.prepareStatement(query_maxPersonID);
		PreparedStatement pstmt_person = con.prepareStatement(query_person);
		PreparedStatement pstmt_client = con.prepareStatement(query_client);
		PreparedStatement pstmt_email = con.prepareStatement(query_email);
		
		// start transaction
		con.setAutoCommit(false);
		
		//check that provided email does not exist in the db
		pstmt_email.setString(1, client.getEmail());
		ResultSet rs_email = pstmt_email.executeQuery();
		if (rs_email.next()){
			con.rollback();
			throw new InvalidInputException("The provided e-mail has already been used for registration.");
			}
		
		
		//get maximum person id 
		ResultSet rs_maxPersonID = pstmt_maxPersonID.executeQuery();
		int max_person_id = 0;
		while(rs_maxPersonID.next()){
			max_person_id = rs_maxPersonID.getInt(1);
		}
		
		rs_maxPersonID.close();
		pstmt_maxPersonID.close();
		
		//set person attributes in ps
		pstmt_person.setInt(1, max_person_id +1);
		pstmt_person.setString(2, client.getFirstName());
		pstmt_person.setString(3, client.getLastName());
		pstmt_person.setString(4, client.getGender());
		pstmt_person.setString(5, client.getTitle());
		pstmt_person.setDate(6, java.sql.Date.valueOf(client.getBirthday()));
		pstmt_person.setString(7, client.getPhoneNo());
		pstmt_person.setString(8, client.getEmail());
		pstmt_person.setString(9, client.getAddress());
		pstmt_person.setString(10, client.getPassword());
		
		//insert person and close ps
		pstmt_person.executeUpdate();
		pstmt_person.close();
		
		//set client attr in ps
		pstmt_client.setInt(1, max_person_id +1);
		pstmt_client.setString(2, client.getCreditCardNo());
		pstmt_client.setDate(3, java.sql.Date.valueOf(client.getExpirationDate()));
		if(client.getIsCompany() == 1){
			pstmt_client.setBoolean(4, true);
		}
		else{
			pstmt_client.setBoolean(4, false);
		}
		
		pstmt_client.setString(5, client.getCompanyName());
		
		//insert client and close ps
		pstmt_client.executeUpdate();
		pstmt_client.close();
		
		//commit changes
		con.commit();
		
		//close connection
		con.close();
		
	}
	
	public static class InvalidInputException extends Throwable {
	
		private static final long serialVersionUID = 1L;

		InvalidInputException(String message){
			super(message);
		}
	}
	
}
