package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.tum.in.dbpra.model.bean.TimetableBean;

public class TimetableDAO extends AbstractDAO {
	public void getTable(ArrayList<TimetableBean> performances, int loginID) throws SQLException {

		//get timetable 
		String tableQuery = "SELECT p.start_play, p.end_play, a.band_name, l.name, l.latitude, l.longitude " +
				"FROM chooses c, accband a, plays p, location l " +
				"WHERE c.band_id = a.band_id and c.band_id = p.band_id and p.location_id = l.location_id and visitor_id = ? " +
				"ORDER BY p.start_play, p.end_play, a.band_name;";
		
		// get connection and start transaction
		Connection con = getConnection();
		con.setAutoCommit(false);

		PreparedStatement pstmt = con.prepareStatement(tableQuery);
		pstmt.setInt(1, loginID);
		ResultSet rs = pstmt.executeQuery();
		
		while (rs.next()) {
			TimetableBean perform = new TimetableBean();
			
			perform.setStartPlay(rs.getTimestamp("start_play"));
			perform.setEndPlay(rs.getTimestamp("end_play"));
			perform.setBandName(rs.getString("band_name"));
			perform.setLocation(rs.getString("name"));
			perform.setLatitude(rs.getBigDecimal("latitude"));
			perform.setLongitude(rs.getBigDecimal("longitude"));

			performances.add(perform);
		}
		rs.close();
		pstmt.close();

		// commit and close the connection
		con.commit();
		con.close();
	}
}
