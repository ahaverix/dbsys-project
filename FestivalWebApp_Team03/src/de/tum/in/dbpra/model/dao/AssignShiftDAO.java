package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.tum.in.dbpra.model.bean.*;

public class AssignShiftDAO extends AbstractDAO{
	public void getPersonnelByShiftTimes(ArrayList<PersonnelBean> personnels, int shift_id) throws SQLException {
		
		//This query selects information about all personnels that are not assigned to shifts overlapping with the shift specified by shift_id
		String query = "WITH searched_shift AS (SELECT start_time, end_time FROM shift WHERE shift_id = ?) " +
				",overlapping_shifts AS (SELECT shift_id, assigned_to FROM shift " +
				"WHERE (start_time < (SELECT start_time FROM searched_shift) AND end_time > (SELECT start_time FROM searched_shift)) " +
				"OR (start_time < (SELECT end_time FROM searched_shift) AND start_time >= (SELECT start_time FROM searched_shift))) " +
				"SELECT p.person_id, p.title, p.first_name, p.last_name, p.birthday, pl.job, pl.experience " +
				"FROM person p JOIN personnel pl ON p.person_id = pl.person_id " +
				"WHERE NOT EXISTS (SELECT * FROM overlapping_shifts s WHERE s.assigned_to = p.person_id);";
		
		//get connection and start transaction
		Connection con = getConnection();
		con.setAutoCommit(false);
		
		//prepare statement, insert the shift_id and execute it
		PreparedStatement pstmt = con.prepareStatement(query);
		pstmt.setInt(1, shift_id);
		ResultSet rs = pstmt.executeQuery();
		
		//add a personnel bean for each entry in the ResultSet to personnels
		while(rs.next()) {
			PersonnelBean personnel = new PersonnelBean();
			personnel.setPerson_id(rs.getInt("person_id"));
			personnel.setTitle(rs.getString("title"));
			personnel.setFirst_name(rs.getString("first_name"));
			personnel.setLast_name(rs.getString("last_name"));
			personnel.setBirthday(rs.getDate("birthday"));
			personnel.setJob(rs.getString("job"));
			personnel.setExperience(rs.getString("experience"));
			personnels.add(personnel);
		}
		
		//commit and close everything
		rs.close();
		pstmt.close();
		con.commit();
		con.close();
	}
}
