package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.tum.in.dbpra.model.bean.*;

public class LoginDAO extends AbstractDAO {
	public void getUser(String email, LoginBean login) throws SQLException {
		// Query to get info about person with certain email
		String queryPerson = "SELECT person_id, first_name, email, password "
				+ "FROM person WHERE email = ?";

		// Get connection and start transaction
		try (Connection con = getConnection();) {
			con.setAutoCommit(false);

			PreparedStatement pstmt;

			// Prepare a statement with the query for user/person
			pstmt = con.prepareStatement(queryPerson);
			pstmt.setString(1, email);

			ResultSet rsUser = pstmt.executeQuery();

			// LoginBean login = new LoginBean();
			while (rsUser.next()) {
				login.setPerson_id(rsUser.getInt("person_id"));
				login.setName(rsUser.getString("first_name"));
				login.setEmail(rsUser.getString("email"));
				login.setPassword(rsUser.getString("password"));
			}

			rsUser.close();
			pstmt.close();

			// Commit and close the connection
			con.commit();
			con.close();
		} catch (SQLException e) {
			System.out.println("Couldnt connect to database.");
			e.printStackTrace();
		}
	}
}
