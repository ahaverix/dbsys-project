package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.tum.in.dbpra.model.bean.BandBean;

public class BandDAO extends AbstractDAO {
	public void getBands(ArrayList<BandBean> chosenBands,
			ArrayList<BandBean> otherBands, int loginID) throws SQLException {

		//get bands that are chosen
		String chosenQuery = "SELECT a.band_id, band_name FROM chooses c, accband a WHERE c.band_id = a.band_id and visitor_id = ?;";
		//get bands that are not chosen
		String otherQuery = "SELECT band_id, band_name " +
				"FROM accband WHERE band_id not in (" +
				"SELECT a.band_id FROM chooses c, accband a " +
				"WHERE c.band_id = a.band_id and visitor_id = ?);";

		// get connection and start transaction
		Connection con = getConnection();
		con.setAutoCommit(false);

		// operations for chosen bands
		PreparedStatement pstmtChosen = con.prepareStatement(chosenQuery);
		pstmtChosen.setInt(1, loginID);
		ResultSet rsChosen = pstmtChosen.executeQuery();
		while (rsChosen.next()) {
			BandBean band = new BandBean();
			band.setBandId(rsChosen.getInt("band_id"));
			band.setBandName(rsChosen.getString("band_name"));
			chosenBands.add(band);
		}
		rsChosen.close();
		pstmtChosen.close();
		
		// operations for other bands
		PreparedStatement pstmtOther = con.prepareStatement(otherQuery);
		pstmtOther.setInt(1, loginID);
		ResultSet rsOther = pstmtOther.executeQuery();
		while (rsOther.next()) {
			BandBean band = new BandBean();
			band.setBandId(rsOther.getInt("band_id"));
			band.setBandName(rsOther.getString("band_name"));
			otherBands.add(band);
		}
		rsOther.close();
		pstmtOther.close();

		// commit and close the connection
		con.commit();
		con.close();
	}
}
