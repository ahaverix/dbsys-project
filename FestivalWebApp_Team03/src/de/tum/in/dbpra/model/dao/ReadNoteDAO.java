package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.tum.in.dbpra.model.bean.NoteInfoBean;

public class ReadNoteDAO extends AbstractDAO {
	public void getNotesByPersonnelID(int personnel_id, ArrayList<NoteInfoBean> notes) throws SQLException{
		//Gets some information regarding all notes that concern the given personnel
		String query = "SELECT n.note_id, n.content, n.posted_at, o.first_name, o.last_name, o.phone_no, o.email " +
				"FROM gets_note gn JOIN note n ON gn.note_id = n.note_id JOIN person o ON n.posted_by = o.person_id " +
				"WHERE gn.personnel_id = ?" +
				"ORDER BY posted_at DESC;";
		
		//Get connection and start transaction
		Connection con = getConnection();
		con.setAutoCommit(false);
		
		//prepate statement, insert personnel_id and execute it
		PreparedStatement pstmt = con.prepareStatement(query);
		pstmt.setInt(1, personnel_id);
		ResultSet rs = pstmt.executeQuery();
		
		//add one NoteInfoBean per line to notes
		while(rs.next()) {
			NoteInfoBean note = new NoteInfoBean();
			note.setNote_id(rs.getInt("note_id"));
			note.setContent(rs.getString("content"));
			note.setPosted_at(rs.getTimestamp("posted_at"));
			note.setOrganizer_name(rs.getString("first_name")+" "+rs.getString("last_name"));			
			note.setOrganizer_phone(rs.getString("phone_no"));
			note.setOrganizer_email(rs.getString("email"));
			notes.add(note);
		}
		
		//commit and close everything
		rs.close();
		pstmt.close();
		con.commit();
		con.close();
	}
}
