package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NoteInsertionDAO extends AbstractDAO {
	public void insertNote(String content, int organizer_id, int [] personnel_ids) throws SQLException{
		//Queries: One for the new note_id, one for the note insertion and one for the gets_note insertions
		String newNote_idQuery = "SELECT max(note_id)+1 AS max FROM note;";
		String noteQuery = "INSERT INTO note(note_id, content, posted_at, posted_by) VALUES(?, ?, now(), ?);";
		String gets_noteQuery = "INSERT INTO gets_note(personnel_id, note_id) VALUES(?, ?);";
		
		//Get connection and start transaction
		Connection con = getConnection();
		con.setAutoCommit(false);
		
		//Get new Note ID (The next higher number or 1 if there is no note yet)
		int note_id = 1;
		PreparedStatement pstmtNewNote_id = con.prepareStatement(newNote_idQuery);
		ResultSet rsNewNote_id = pstmtNewNote_id.executeQuery();
		if(rsNewNote_id.next()){
			note_id = rsNewNote_id.getInt("max");
		}
		rsNewNote_id.close();
		pstmtNewNote_id.close();
		
		//Note Insertion
		PreparedStatement pstmtNote = con.prepareStatement(noteQuery);
		pstmtNote.setInt(1, note_id);		
		pstmtNote.setString(2, content);
		pstmtNote.setInt(3, organizer_id);		
		int linesAffected = pstmtNote.executeUpdate();
		pstmtNote.close();
		//Rollback if more or less than one line was affected
		if(linesAffected != 1){
			con.rollback();
			con.close();
			throw new SQLException("Insertion Error: The insertion of the note affected not exactly 1 row");
		}
		
		//Gets_note Insertions
		PreparedStatement pstmtGets_note = con.prepareStatement(gets_noteQuery);
		pstmtGets_note.setInt(2, note_id);
		for (int i = 0; i < personnel_ids.length; i++){
			//Make sure to not assign the note to the same personnel twice
			if(i==0 || personnel_ids[i]!=personnel_ids[i-1]){
				pstmtGets_note.setInt(1, personnel_ids[i]);
				linesAffected = pstmtGets_note.executeUpdate();
				//Rollback if more or less than one line was affected
				if(linesAffected != 1){
					pstmtGets_note.close();
					con.rollback();
					con.close();
					throw new SQLException("Insertion Error: The insertion of a gets_note entry affected not exactly 1 row");
				}
			}
		}
		pstmtGets_note.close();
		
		//Commit and close connection
		con.commit();
		con.close();
	}
}
