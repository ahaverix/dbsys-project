package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.tum.in.dbpra.model.bean.*;

public class ShiftDAO extends AbstractDAO{
	public void getShiftsbyAssignment(ArrayList<ShiftBean> assignedShifts, ArrayList<ShiftBean> unassignedShifts) throws SQLException {
		
		//gets all shifts that are currently unassigned
		String unassignedQuery = "SELECT s.shift_id, s.start_time, s.end_time, s.task, l.type AS location " +
				"FROM shift s JOIN location l ON s.executed_in = l.location_id " +
				"WHERE s.assigned_to IS NULL " +
				"ORDER BY shift_id;";
		//gets all shifts that are currently assigned
		String assignedQuery = "SELECT s.shift_id, s.start_time, s.end_time, s.task, l.type AS location, p.first_name, p.last_name " +
				"FROM shift s JOIN location l ON s.executed_in = l.location_id JOIN person p ON s.assigned_to = p.person_id " +
				"ORDER BY shift_id;";
		
		//get connection and start transaction
		Connection con = getConnection();
		con.setAutoCommit(false);
		
		/* Do the following for both assigned and unassigned shifts:
		 * - prepare the statement and execute it
		 * - add a ShiftBean for each entry in the ResultSet to the corresponding ArrayList
		 * - close ResultSet and the PreparedStatement
		 * */		
		//operations for unassigned shifts
		PreparedStatement pstmtUnassigned = con.prepareStatement(unassignedQuery);
		ResultSet rsUnassigned = pstmtUnassigned.executeQuery();
		while(rsUnassigned.next()) {
			ShiftBean shift = new ShiftBean();
			shift.setShift_id(rsUnassigned.getInt("shift_id"));
			shift.setStart_time(rsUnassigned.getTimestamp("start_time"));
			shift.setEnd_time(rsUnassigned.getTimestamp("end_time"));
			shift.setTask(rsUnassigned.getString("task"));
			shift.setLocation(rsUnassigned.getString("location"));
			unassignedShifts.add(shift);
		}
		rsUnassigned.close();
		pstmtUnassigned.close();		
		//operations for assigned shifts
		PreparedStatement pstmtAssigned = con.prepareStatement(assignedQuery);
		ResultSet rsAssigned = pstmtAssigned.executeQuery();
		while(rsAssigned.next()) {
			ShiftBean shift = new ShiftBean();
			shift.setShift_id(rsAssigned.getInt("shift_id"));
			shift.setStart_time(rsAssigned.getTimestamp("start_time"));
			shift.setEnd_time(rsAssigned.getTimestamp("end_time"));
			shift.setTask(rsAssigned.getString("task"));
			shift.setLocation(rsAssigned.getString("location"));
			shift.setAssigned_to(rsAssigned.getString("first_name")+" "+rsAssigned.getString("last_name"));
			assignedShifts.add(shift);
		}
		rsAssigned.close();
		pstmtAssigned.close();
		
		//commit and close the connection
		con.commit();
		con.close();
	}
}
