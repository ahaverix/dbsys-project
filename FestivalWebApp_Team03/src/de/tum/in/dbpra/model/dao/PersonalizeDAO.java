package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.tum.in.dbpra.model.bean.TicketBean;
import de.tum.in.dbpra.model.bean.VisitorBean;

public class PersonalizeDAO extends AbstractDAO{
	
	//get list of booked and not assigned tickets
	public ArrayList<TicketBean> getUnpersonalizedTickets(String client_email) throws SQLException, ClassNotFoundException {
		
		//query
		String query_unpers_tickets = "SELECT * FROM ticket WHERE client_id = (select person_id from person where email = ?) and visitor_id is null;";
		
		//connection
		Connection con = getConnection();
		
		//ps
		PreparedStatement ps_unpers_tickets = con.prepareStatement(query_unpers_tickets);
		ps_unpers_tickets.setString(1, client_email);
		
		// start transaction
		con.setAutoCommit(false);
		
		//execute query
		ResultSet rs_unpers_tickets = ps_unpers_tickets.executeQuery();
		con.commit();
		
		//add tickets to list
		ArrayList<TicketBean> list_unpers_tickets = new ArrayList<TicketBean>();
			while(rs_unpers_tickets.next()){
				TicketBean ticket = new TicketBean();
				
				ticket.setTicketID(rs_unpers_tickets.getInt("ticket_id"));
				ticket.setType(rs_unpers_tickets.getString("type"));
				ticket.setArea(rs_unpers_tickets.getString("area"));
				if(rs_unpers_tickets.getString("vip").equals("t")){
					ticket.setVip(1);
				}
				else{
					ticket.setVip(0);
				}				
				list_unpers_tickets.add(ticket);	
			}			
		
		//close rs, ps and con
		rs_unpers_tickets.close();
		ps_unpers_tickets.close();
		con.close();
		
		//return list of booked and not assigned tickets
		return list_unpers_tickets;
	}
	
//assign a ticket that you bought to yourself	
public void assignTicketToSelf(int ticket_id, String client_email) throws InvalidInputException, SQLException, ClassNotFoundException {
		
		//queries
		String query_client_id = "select person_id from person where email = ?;";
		String query_ticketToSelf = "update ticket set visitor_id = ? where ticket_id = ? ;"; //actual assignment
		String query_countClientTickets = "select count(*) from ticket where visitor_id = ?;"; //for assertion that the client does not have more that 6 tickets
		String query_booked_tickets = "select ticket_id from ticket where client_id = ? and visitor_id is null;"; //get unassigned tickets
		String query_insert_visitor = "insert into visitor values(?, null);"; //create visitor
		String query_visitor = "select * from visitor where person_id = ?;"; 
		
		//connection
		Connection con = getConnection();
		
		//ps
		PreparedStatement ps_client_id = con.prepareStatement(query_client_id);
		PreparedStatement ps_booked_tickets = con.prepareStatement(query_booked_tickets);
		PreparedStatement ps_ticketToSelf = con.prepareStatement(query_ticketToSelf);
		PreparedStatement ps_countClientTickets = con.prepareStatement(query_countClientTickets);
		PreparedStatement ps_insert_visitor = con.prepareStatement(query_insert_visitor);
		PreparedStatement ps_visitor = con.prepareStatement(query_visitor);
		
		//check if ticket_id is provided
		if(ticket_id == -1){
			throw new InvalidInputException("No ticket number provided.");
		}
		
		// start transaction
		con.setAutoCommit(false);
		
		//get client_id
		int client_id = 0;
		ps_client_id.setString(1, client_email);
			
			//result set
		ResultSet rs_client_id = ps_client_id.executeQuery();
		if(rs_client_id.next()){
			client_id = rs_client_id.getInt(1);
		}
			//close rs and ps
		rs_client_id.close();
		ps_client_id.close();
		
		
		//set attr in ps
		ps_insert_visitor.setInt(1, client_id);
		ps_booked_tickets.setInt(1, client_id);
		ps_ticketToSelf.setInt(1, client_id);
		ps_ticketToSelf.setInt(2, ticket_id);
		ps_countClientTickets.setInt(1, client_id);
		
		
		//check if ticket_id of the ticket that has to be personalized is valid
			//get unpersonalized tickets
		ResultSet rs_booked_tickets = ps_booked_tickets.executeQuery();
		ArrayList<Integer> list_booked_tickets = new ArrayList<Integer>();
		
			//add tickets to list
		while(rs_booked_tickets.next()){	
			list_booked_tickets.add(rs_booked_tickets.getInt(1));	
		}
		if(!list_booked_tickets.contains(ticket_id)){
			con.rollback();
			throw new InvalidInputException("The provided ticket number is not one of your booked and not assigned tickets.");
		}
			//close rs and ps
		rs_booked_tickets.close();
		ps_booked_tickets.close();
		
		
		//check if the client has less than 6 tickets assigned to self
			//get number of tickts assigned to the specified user
		ResultSet rs_countClientTickets = ps_countClientTickets.executeQuery();
		
		int no_client_tickets = 0;
		if(rs_countClientTickets.next()){
			no_client_tickets = rs_countClientTickets.getInt(1);
		}
		if(no_client_tickets >5){
			con.rollback();
			throw new InvalidInputException("You already have 6 tickets assigned to yourself.");
		}
			//close rs and ps
		rs_countClientTickets.close();
		ps_countClientTickets.close();
		
		
	
		
		//insert client into visitor table
		ps_visitor.setInt(1, client_id);
			//check if visitor exists
		ResultSet rs_visitor = ps_visitor.executeQuery();
		if(!rs_visitor.next()){
			//insert visitor
			ps_insert_visitor.executeUpdate();
		}
			//close rs and ps
		rs_visitor.close();
		ps_visitor.close();
		ps_insert_visitor.close();
		
		
		//assign Ticket to self
		ps_ticketToSelf.executeUpdate();
		
			//close ps		
		ps_ticketToSelf.close();
		
		
		//commit changes
		con.commit();

		//close connection
		con.close();
		
	
	}
	
	
	
	
	//insert visitor in the visitor table
	public void insertVisitor(VisitorBean visitor) throws  InvalidInputException, SQLException, ClassNotFoundException {
		
		//check that all not-null fields are there
		if(visitor.getFirstName().isEmpty() || visitor.getLastName().isEmpty()  || visitor.getGender() == null ||
		   visitor.getBirthday().isEmpty()  || visitor.getEmail().isEmpty()){
			   throw new InvalidInputException("The following fields cannot be empty: first name, last name, gender," +
			   		" date of birth, email.");
		}
		
		//check date is set
		if(visitor.getBirthday().contains("s")){
			throw new InvalidInputException("Date of birth not set. Pleas set the visitor's date of birth.");
		}
			
		//check birthday is valid
		int b_year = Integer.parseInt(visitor.getBirthday().substring(0, 4));
		if((visitor.getBirthday().substring(5, 7).equals("02") && visitor.getBirthday().substring(8, 10).equals("31")) ||
				(visitor.getBirthday().substring(5, 7).equals("02") && visitor.getBirthday().substring(8, 10).equals("30")) ||	
				(visitor.getBirthday().substring(5, 7).equals("04") && visitor.getBirthday().substring(8, 10).equals("31")) ||	
				(visitor.getBirthday().substring(5, 7).equals("06") && visitor.getBirthday().substring(8, 10).equals("31")) ||
				(visitor.getBirthday().substring(5, 7).equals("09") && visitor.getBirthday().substring(8, 10).equals("31")) ||
				(visitor.getBirthday().substring(5, 7).equals("11") && visitor.getBirthday().substring(8, 10).equals("31")) 
				){
			throw new InvalidInputException("Date " + visitor.getBirthday() + " is invalid.");
		}
		else if(b_year%4 !=0 && visitor.getBirthday().substring(8, 10).equals("29")){
			throw new InvalidInputException("Date " + visitor.getBirthday() + " is invalid.");		
		}
	
		//check phone number consists only of numbers
		if (!(visitor.getPhoneNo().matches("[0-9]+") || visitor.getPhoneNo().isEmpty())){
			throw new InvalidInputException("The phone number can contain only numbers (no letters and other signs).");
		}
		
		//check for right e-mail format	
		String email =  visitor.getEmail();
		int ct =   email.length() - email.replace("@", "").length();
		if(ct != 1 || !visitor.getEmail().contains(".")){
			throw new InvalidInputException("Wrong e-mail format. The e-mail has to be of the following form: \'example@abc.xy\'. ");
		}
		
			
		//queries
		String query_maxPersonID = "select max(person_id) from person;";
		String query_person = "insert into person values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		String query_visitor = "insert into visitor values (?, ?)";
		String query_email = "select * from person where email = ?;";
		
		//connection
		Connection con = getConnection();
		
		//ps
		PreparedStatement pstmt_maxPersonID = con.prepareStatement(query_maxPersonID);
		PreparedStatement pstmt_person = con.prepareStatement(query_person);
		PreparedStatement pstmt_visitor = con.prepareStatement(query_visitor);
		PreparedStatement pstmt_email = con.prepareStatement(query_email);
		
		// start transaction
		con.setAutoCommit(false);
		
		//assert that the provide email has not been used yet
		pstmt_email.setString(1, visitor.getEmail());
		ResultSet rs_email = pstmt_email.executeQuery();
		if (rs_email.next()){
			con.rollback();
			throw new InvalidInputException("A visitor with the e-mail " +  visitor.getEmail() + " already exists.");
			}
			//close rs and ps
		rs_email.close();
		pstmt_email.close();
		
		
		//get maximum person id for computing the primary key of the visitor
		ResultSet rs_maxPersonID = pstmt_maxPersonID.executeQuery();
		int max_person_id = 0;
			//get query resuts
		while(rs_maxPersonID.next()){
			max_person_id = rs_maxPersonID.getInt(1);
		}
			//close rs and ps
		rs_maxPersonID.close();
		pstmt_maxPersonID.close();
		
		
		//insert visitor into person table
			//set ps attr
		pstmt_person.setInt(1, max_person_id +1);
		pstmt_person.setString(2, visitor.getFirstName());
		pstmt_person.setString(3, visitor.getLastName());
		pstmt_person.setString(4, visitor.getGender());
		pstmt_person.setString(5, visitor.getTitle());
		pstmt_person.setDate(6, java.sql.Date.valueOf(visitor.getBirthday()));
		pstmt_person.setString(7, visitor.getPhoneNo());
		pstmt_person.setString(8, visitor.getEmail());
		pstmt_person.setString(9, visitor.getAddress());
		pstmt_person.setString(10, visitor.getPassword());
		
			//execute insertion and close ps
		pstmt_person.executeUpdate();
		pstmt_person.close();
		
		//insert visitor into visitor table
			//set ps attr
		pstmt_visitor.setInt(1, max_person_id +1);
		pstmt_visitor.setString(2, visitor.getDisability());
	
			//execute insertion and close ps
		pstmt_visitor.executeUpdate();
		pstmt_visitor.close();
		
		//commit changes
		con.commit();
		
		
		//close connection
		con.close();
		
	}
	
	
	
	
	//assign ticket to an existing visitor
	public void personaizeTicket(int ticket_id, String visitor_email, String client_email) throws  InvalidInputException, SQLException, ClassNotFoundException {
		
		//queries
		String query_client_id = "select person_id from person where email = ?;";	
		String query_visitor_email = "select * from person where email = ?;";
		String query_visitor_exists = "select * from person p, visitor v" +
				" where p.person_id = v.person_id and  p.email = ?;";
		String query_personalize = "update ticket set visitor_id = (select person_id from person where email = ?) where ticket_id = ?;";
		String query_booked_tickets = "select ticket_id from ticket where client_id = ? and visitor_id is null;";
		String query_countVisitorTickets = "select count(*) from ticket t, visitor v, person p " +
				" where t.visitor_id = v.person_id and v.person_id = p.person_id " +
				"and p.email = ?;"; //for assertion that the visitor does not have more that 6 tickets
		
		//connection
		Connection con = getConnection();
		
		//ps
		PreparedStatement ps_client_id = con.prepareStatement(query_client_id);
		PreparedStatement pstmt_visitor_email = con.prepareStatement(query_visitor_email);
		PreparedStatement pstmt_visitor_exists = con.prepareStatement(query_visitor_exists);
		PreparedStatement pstmt_personalize = con.prepareStatement(query_personalize);
		PreparedStatement ps_booked_tickets = con.prepareStatement(query_booked_tickets);
		PreparedStatement ps_countVisitorTickets = con.prepareStatement(query_countVisitorTickets);
		
		//check if ticket_id is provided
				if(ticket_id == -1){
					throw new InvalidInputException("No ticket number provided.");
				}
				
		//check if visitr email is provided
				if(visitor_email.isEmpty() ){
					 throw new InvalidInputException("No visitor e-mail provided.");
						}
				
		// start transaction
		con.setAutoCommit(false);
		
		//get client_id
				int client_id = 0;
				ps_client_id.setString(1, client_email);
				ResultSet rs_client_id = ps_client_id.executeQuery();
					//get query results
				if(rs_client_id.next()){
					client_id = rs_client_id.getInt(1);
				}
					//close rs and ps
				rs_client_id.close();
				ps_client_id.close();
				
				
		
		//check if ticket_id is valid
				ps_booked_tickets.setInt(1, client_id);
				ResultSet rs_booked_tickets = ps_booked_tickets.executeQuery();
				ArrayList<Integer> list_booked_tickets = new ArrayList<Integer>();
				
				//get list of booked and not personalized
				while(rs_booked_tickets.next()){
							
					list_booked_tickets.add(rs_booked_tickets.getInt(1));	
				}
				if(!list_booked_tickets.contains(ticket_id)){
					con.rollback();
					throw new InvalidInputException("The provided ticket number is not one of your booked and not assigned tickets.");
				}
				
				//close rs and ps
				rs_booked_tickets.close();
				ps_booked_tickets.close();
				
		
		//check if email exists in the database
		pstmt_visitor_email.setString(1, visitor_email);
		ResultSet rs_visitor_email = pstmt_visitor_email.executeQuery();
			//get query results and throw exception if necessary
		if (!rs_visitor_email.next()){
			con.rollback();
			throw new InvalidInputException("There is no visitor registered with the email " + visitor_email + ". Please register the visitor first.");
			}
			//close rs and ps
		rs_visitor_email.close();
		pstmt_visitor_email.close();
		
		
		//check if visitor exists in the database
				pstmt_visitor_exists.setString(1, visitor_email);
				ResultSet rs_visitor_exists = pstmt_visitor_exists.executeQuery();
					//get query results and throw exception if necessary
				if (!rs_visitor_exists.next()){
					con.rollback();
					throw new InvalidInputException("There is no visitor registered with the email " + visitor_email + ". Please register the visitor first.");
					}
					//close rs and ps
				rs_visitor_exists.close();
				pstmt_visitor_exists.close();
		
		
		//check if the visitor has less than 6 tickets assigned to him
			//set ps attr
		ps_countVisitorTickets.setString(1, visitor_email);
		
			//get number of tickts assigned to the specified user
		ResultSet rs_countVisitorTickets = ps_countVisitorTickets.executeQuery();
	
		int no_visitor_tickets = 0;
		if(rs_countVisitorTickets.next()){
			no_visitor_tickets = rs_countVisitorTickets.getInt(1);
		}
		if(no_visitor_tickets >5){
			con.rollback();
			throw new InvalidInputException("The visitor already has 6 tickets assigned to him.");
		}
			//close rs and ps
		rs_countVisitorTickets.close();
		ps_countVisitorTickets.close();
		
		
		//personalize
			//set ps attr
		pstmt_personalize.setString(1, visitor_email);	
		pstmt_personalize.setInt(2, ticket_id);
			
			//execute query and close ps
		pstmt_personalize.executeUpdate();
		pstmt_personalize.close();
		
		//commit changes
		con.commit();
		
		//close connection
		con.close();
		
	}
	

	
	class InvalidInputException extends Throwable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		InvalidInputException(String message){
			super(message);
		}
	}
	


}

