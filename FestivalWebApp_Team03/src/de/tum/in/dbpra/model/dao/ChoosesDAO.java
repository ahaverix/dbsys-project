package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ChoosesDAO extends AbstractDAO{
	public void insertChooses(int loginID, int[] band_ids) throws SQLException {
		//delete old chooses tuples for id
		String queryDelete = "DELETE from chooses WHERE visitor_id = ?";
		//insert new chooses tuples for id
		String queryInsert = "INSERT into chooses values (?, ?);";
		
		//Get connection and start transaction
		Connection con = getConnection();
		con.setAutoCommit(false);

		//Prepare a statement with the query for deletion
		PreparedStatement pstmt1;
		pstmt1 = con.prepareStatement(queryDelete);
		pstmt1.setInt(1, loginID);
		int rowsDeleted = pstmt1.executeUpdate();
		if (rowsDeleted > 0) {
		    System.out.println("A chooses tuple was deleted successfully!");
		}
		pstmt1.close();
		
		//Prepare a statement with the query for insertion for each band_id
		for (int i=0; i<band_ids.length; i++){
			PreparedStatement pstmt2 = con.prepareStatement(queryInsert);
			pstmt2.setInt(1, loginID);
			pstmt2.setInt(2, band_ids[i]);
					
			int rowsInserted2 = pstmt2.executeUpdate();
			if (rowsInserted2 > 0) {
			    System.out.println("A new chooses tuple was inserted successfully!");
			}
			pstmt2.close();			
		}
		
		//Commit and close the connection
		con.commit();
		con.close();
	}
}
