package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.tum.in.dbpra.model.bean.TicketBean;

public class TicketDAO extends AbstractDAO{
	
	//insert ticket into database
	public void insertTicket(TicketBean ticket, String client_email) throws InvalidTicketInputException, SQLException, ClassNotFoundException {
		
		//check that all not-null fields are there
				if(ticket.getType().isEmpty() || ticket.getArea() == null  || ticket.getVip() == -1){
					   throw new InvalidTicketInputException("The following fields cannot be empty: type, area, vip.");
				}
		
		//queries
		String query_maxTicketID = "select max(ticket_id) from ticket;"; //get maximum ticket id for computing the primary key
		String query_getClientID = "SELECT person_id FROM person WHERE email = ?;"; //get person id of the logged in client
		String query_no_booked_tickets = "select count(*) from ticket where client_id = ?;"; //for assertion that client books no more than 20 tickets
		String query_insertTicket = "insert into ticket values (?, ?, ?, ?, ?, ?, null, ?::numeric::money, now());"; 
		
		//connection
		Connection con = getConnection();
		
		
		//ps
		PreparedStatement pstmt_maxTicketID = con.prepareStatement(query_maxTicketID);
		PreparedStatement pstmt_getClientID = con.prepareStatement(query_getClientID);
		PreparedStatement pstmt_no_booked_tickets = con.prepareStatement(query_no_booked_tickets);
		PreparedStatement pstmt_insertTicket = con.prepareStatement(query_insertTicket);
		
		// start transaction
		con.setAutoCommit(false);
		
		
		//get max TicketID
		ResultSet rs_maxTicketID = pstmt_maxTicketID.executeQuery();
		int max_ticket_id = 0;
		
			//get query results
		while(rs_maxTicketID.next()){
			max_ticket_id = rs_maxTicketID.getInt(1);
		}
			//close rs and ps
		rs_maxTicketID.close();
		pstmt_maxTicketID.close();
		
		
		//get clientID.
			//set ps attr
		pstmt_getClientID.setString(1, client_email);
	
		int client_id = 0;
		
		  //get query results
		ResultSet rs_getClientID = pstmt_getClientID.executeQuery();
		if(rs_getClientID.next()) {
			client_id = rs_getClientID.getInt("person_id");
			ticket.setClientID(client_id);
		} 
			//close rs and ps
		rs_getClientID.close();
		pstmt_getClientID.close();
		
		
		
		//check that the client has not booked 20 tickets yet
			//set ps attr
		pstmt_no_booked_tickets.setInt(1, client_id);
		
		int no_tickets = 0;
		
			//get query results
		ResultSet rs_no_booked_tickets = pstmt_no_booked_tickets.executeQuery();
		if(rs_no_booked_tickets.next()) {
			no_tickets = rs_no_booked_tickets.getInt(1);;
		} 
		
			//close rs and ps
		rs_no_booked_tickets.close();
		pstmt_no_booked_tickets.close();
		
			//throw exception if needed
		if(no_tickets>19){
			con.rollback();
			throw new InvalidTicketInputException("You already booked 20 tickets, which is the maximum number of tickets that can be booked.");
		}
		
		
		//insert Ticket
			//set ps attr
		pstmt_insertTicket.setInt(1, max_ticket_id + 1);
		pstmt_insertTicket.setString(2, ticket.getType());
		pstmt_insertTicket.setDate(3, java.sql.Date.valueOf(ticket.getValidFrom()));
		pstmt_insertTicket.setString(4, ticket.getArea());
	
		if(ticket.getVip() == 1){
			pstmt_insertTicket.setBoolean(5, true);
		}
		else {
			pstmt_insertTicket.setBoolean(5, false);
		}
		
		pstmt_insertTicket.setInt(6, ticket.getClientID());
		pstmt_insertTicket.setBigDecimal(7, ticket.getPrice());
		
			//execute insertion and close ps
		pstmt_insertTicket.executeUpdate();
		pstmt_insertTicket.close();
	
		
		//commit changes
		con.commit();
		
		//close con
		con.close();
		
	}
	
	public static class InvalidTicketInputException extends Throwable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		InvalidTicketInputException(String message){
			super(message);
		}
	}
}
