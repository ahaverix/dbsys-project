package de.tum.in.dbpra.model.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern; //for testing see end;

import de.tum.in.dbpra.model.bean.BillBean;
import de.tum.in.dbpra.model.bean.InventoryShopBean;
import de.tum.in.dbpra.model.bean.PurchaseProductsBean;

public class PurchaseDAO extends AbstractDAO {
	/*
	 * HINT: read also the //System.out.println(); They have been used for debugging and are very helpful in understanding the code. 
	 * HINT: Helping methods are shown near the end above TESTING.
	 */
	
	

	/*
	 * The insert purchase method. This method can handle multiple inserts at the same time and returns a string which either shows
	 * the exceptions thrown by the Trigger in the Database, or the Bill which is then displayed via the methods in the JSP.
	 * 
	 * The trigger automatically checks if a purchase can be made and also updates the wristband balance. 
	 */
	public String insertPurchase(String shopID, String[] amount, String wristbandID, String[] selectedMerchandise){
		String response = null; //this is the response
		if(selectedMerchandise==null){
			response= "You have not selected any items. Please try again";
			return response;
		}
			int[] selectedMerchandiseID = StringArrayToInt(selectedMerchandise); 
			BigDecimal totalCost = new BigDecimal(0);
			boolean[] inserted = new boolean[selectedMerchandiseID.length]; //We will use this array to check every insert if successful
			List<BillBean> fullBill = new ArrayList<BillBean>(); 
			String[] realAmount = new String[selectedMerchandiseID.length]; 
			try {
				/* we find the position of the selected Merchandise in the List of products of a specific shop and then fill
				 * the realAmount[] with the correct data at the right places
				 */
				realAmount =realAmount((testMerchandiseIDPos(ShopProductList(shopID), selectedMerchandiseID)), amount);
				if(testTheRest((testMerchandiseIDPos(ShopProductList(shopID), selectedMerchandiseID)), amount)){
					if(!isStringInt(wristbandID) && !areStringArrayElementsPositiveIntegers(realAmount)){
						response = "You have entered an invalid wristbandID and amount(s). Please retry inserting new values";
			   	 	}
					else if(!isStringInt(wristbandID) && areStringArrayElementsPositiveIntegers(realAmount)){
						response = "You have entered an invalid wristbandID. Please retry inserting a new value";
			   	 	}
					else if(isStringInt(wristbandID) && !areStringArrayElementsPositiveIntegers(realAmount)){
						response = "You have entered invalid amount(s). Please retry inserting new values";
					}
				 	
			   	 	else{
			   	 		//Has already been asked, but it gives feedback that negative integers are not permitted.
			   	 		if(areStringArrayElementsPositiveIntegers(realAmount)){
			   	   	 		System.out.println("Input was correct.");
			   	   	 		try (		
			   	   	    	 	Connection connection = getConnection();  
			   			        ){
			   	   	 			connection.setAutoCommit(false);
			   	   	 			System.out.println("connection established");
			   	   	 			//check wether the wristbandID is correct.
			   					PreparedStatement wristbandStatement = connection.prepareStatement("select exists(select 1 from wristband where wristband_id=?)");
			   					wristbandStatement.setInt(1, Integer.parseInt(wristbandID));
			   					boolean checkedWristband = false;
			   					ResultSet wristbandResult = wristbandStatement.executeQuery();
			   					
			   					while (wristbandResult.next()){
			   	    				checkedWristband = wristbandResult.getBoolean(1);
//			   	    				System.out.println(checkedWristband);
			   					}
			   					wristbandStatement.close();
			   					wristbandResult.close();
			   					if(checkedWristband){
						System.out.println("wristband found continuing with The creation of the Full Bill.");

			   						for (int i = 0; i < selectedMerchandiseID.length; i++) {
			   							BillBean bill = new BillBean();
			   							System.out.println("creating partialBill: "+i +" now");
			   							bill.setAmount(Integer.parseInt(realAmount[i]));
			   							bill.setTime(new Timestamp(System.currentTimeMillis()));
			   							PreparedStatement billStatement = connection.prepareStatement("select name, price::money::numeric FROM merchandise WHERE merchandise_id = ?");
			   							billStatement.setInt(1, selectedMerchandiseID[i]);
			   							System.out.println("Set the merchandiseID in the query.");
			   							ResultSet billResult = billStatement.executeQuery();
			   							System.out.println("executing Query.");
			   							
			   							while (billResult.next()){
					   						bill.setMerchandiseName(billResult.getString(1));
					   						bill.setMerchandisePrice(billResult.getBigDecimal(2));
					   					}
			   							bill.setTotalPrice(bill.getMerchandisePrice(), bill.getAmount());
			   							System.out.println("bill Created.");
			   							billStatement.close();
			   							billResult.close();
			   							fullBill.add(bill);
									}
			   			System.out.println("Created Full Bill. Continuing with insertation to the Database.");
											   					
					   				for (int i = 0; i < fullBill.size(); i++) {
					   					PreparedStatement insertStatement = connection.prepareStatement("Insert into purchase VALUES " +
													"(?, ?, ?, ?, ?)");
					   					insertStatement.setInt(1, Integer.parseInt(wristbandID));
				   						insertStatement.setInt(2, selectedMerchandiseID[i]);
				   						insertStatement.setInt(3, Integer.parseInt(shopID));
				   						insertStatement.setTimestamp(4, fullBill.get(i).getTime());
				   						insertStatement.setInt(5, Integer.parseInt(realAmount[i]));;
				   						try{
				   							insertStatement.executeUpdate();
				   							fullBill.get(i).setBilling(fullBill.get(i).getTime()+"$"+fullBill.get(i).getMerchandiseName()
				   									 + "$"+fullBill.get(i).getMerchandisePrice()+"$"+fullBill.get(i).getAmount()+"$"+fullBill.get(i).getTotalPrice()+"$");
				   							inserted[i]= true;
				   						}
				   						catch (Exception e) {
				   							response = e.getMessage(); //thank the trigger
				   							break;
				   						}
				   						insertStatement.close();
				   					}
					   				//if all queries work then we commit
					   				if(assertQueries(inserted)){
									connection.commit();
					   				connection.close();
					   				}
					   				//Else we rollback.
					   				else{
					   				connection.rollback();
					   				connection.close();
					   				}
					   			}	
			   					else{
			   						response = "Could not find the selected WristbandID in the Database. Please try again.";
			   					}
			   	   	 		} catch (SQLException e) {
			   					response = "Could not connect to database. Please try again later.";
			   				}
			   	 			
			   	 		}
			   	 		else{
			   	 			response = "Cannot enter negative amount or 0. Please try again.";
			   	 		}

			   	 	}
				}
				else{
					response = "You have a missmatch between the selected Merchandises and their respective amount. Please try again.";
				}
			} catch (SQLException e1) {
				System.out.println("Pill Te somes po ktu si u fute...");
				e1.printStackTrace();
				response= "An unspecified error has eccoured. Please contact the staff members.";
				return response;
			}
			
			//If insertion was successful, we generate the Bill 
		if(assertQueries(inserted)){
			response ="Purchase has been completed.$"; 
			for (int i = 0; i < inserted.length; i++) {
				response += fullBill.get(i).getBilling();
				totalCost = totalCost.add(fullBill.get(i).getTotalPrice());
			}
			response += totalCost.toString()+"$";
		}
		//We return the response, whatever it may be. 
		return response;

	}
	//Function to get the list of Shops 
	public List<InventoryShopBean> shopList() throws SQLException {
        List<InventoryShopBean> shops = new ArrayList<InventoryShopBean>();

        try (Connection connection = getConnection()){
        	connection.setAutoCommit(false);
            PreparedStatement statement = connection.prepareStatement("SELECT shop_id, shop_name FROM shop");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                InventoryShopBean shop = new InventoryShopBean();
                shop.setShopID(resultSet.getInt("shop_id"));
                shop.setShopName(resultSet.getString("shop_name"));
                shops.add(shop);
            }
            statement.close();
            resultSet.close();
            connection.commit();
            connection.close();
        }
//        System.out.println("Data Fetched");
        return shops;
    }
	

	public List<PurchaseProductsBean> ShopProductList(String shopID) throws SQLException {
		List<PurchaseProductsBean> shopProducts= new ArrayList<PurchaseProductsBean>(); 
		
		try (Connection connection = getConnection()) {
			connection.setAutoCommit(false);
			PreparedStatement statement = connection.prepareStatement("SELECT i.merchandise_id, m.name, m.price::money::numeric" +
				" FROM inventory i, merchandise m WHERE" +
				" m.merchandise_id = i.merchandise_id and" +
				" i.shop_id = ?");
				statement.setInt(1,Integer.parseInt(shopID));
				ResultSet resultSet = statement.executeQuery();
	            while (resultSet.next()) {
	                PurchaseProductsBean product = new PurchaseProductsBean();
	                product.setMerchandiseID(resultSet.getInt("merchandise_id"));
	                product.setMerchandiseName(resultSet.getString("name"));
	                product.setMerchandisePrice(resultSet.getBigDecimal("price"));
	                shopProducts.add(product);
	            }
	            statement.close();
	            resultSet.close();
	            connection.commit();
	            connection.close();
	        }
//	        System.out.println("Data Fetched");
	        return shopProducts;
	    }
	
	/*Helping Methods
	 * 
	 */
	
	//this method is used to check wether all Purchase entries were successfull. All have to be for them to be inserted into the Database and for the bill to show.
	public boolean assertQueries(boolean[] s){
		boolean a = true;
		for (int i = 0; i < s.length; i++) {
			a = a && s[i];
		}
		return a;
	}

	//A simple function which checks wether a string is an integer
	public boolean isStringInt(String s)
	{
	    try
	    {
	        Integer.parseInt(s);
	        return true;
	    } catch (NumberFormatException ex)
	    {
	        return false;
	    }
	}
	//A simple method which chekcs wether the String elements are positive integers-
	public boolean areStringArrayElementsPositiveIntegers(String[] s){
		boolean positive = true;
		for (int i = 0; i < s.length; i++) {
			try{
			positive = positive && (Integer.parseInt(s[i])>0);
			}
			catch(Exception e){
				positive = positive && false;	
			}
		}
		return positive;
	}
	//This function generates the correct Amount Array by using the positions found by the testMerchandiseIDPos
	public String[] realAmount(int[] positions, String[] amounts){
		String[] realAmounts = new String[positions.length];
		
		for (int i = 0; i < positions.length; i++) {
			for (int j = 0; j < amounts.length; j++) {
				if (positions[i]==j){
					realAmounts[i]=amounts[j];
					break;
				}
			}
		}
		return realAmounts;	
	}
	//This function checks wether extra data has been given by the user, in which case he would have to be warned
	public boolean testTheRest(int[] positions, String[] amounts){
		boolean noExtraData = true;
		int values = 0;
		
		for (int i = 0; i < amounts.length; i++) {
			if(!amounts[i].trim().isEmpty()){
				values ++;
				//System.out.println("$"+amounts[i] +"$");
			}
		}
//		System.out.println(values);
		if(values != positions.length){
			noExtraData = false;
		}
		return noExtraData;
		
	}
	// This function creates an array listing the positions of products in a shop.
	public int[] testMerchandiseIDPos(List<PurchaseProductsBean> shopProductIds, int[] selected){
		int[] pos = new int[selected.length];
		for (int i = 0; i < selected.length; i++) {
			for (int j = 0; j < shopProductIds.size(); j++) {
				if(selected[i]==shopProductIds.get(j).getMerchandiseID()){
					pos[i]=j;
					break;
				}
			}
			
		}
		return pos;
	}
	
	//A simple method which turns a String array into a integer array
	public int[] StringArrayToInt(String[] b){
		int[] a= new int[b.length];
		for (int i = 0; i < b.length; i++) {
			a[i] = Integer.parseInt(b[i]);	
		}
		return a;
	}
//	TESTING

//	public static void main(String[] args) {
//		PurchaseDAO DAO = new PurchaseDAO();
//		String[] merchid = new String[3];
//		merchid[0]="3";
//		merchid[1]="4";
//		merchid[2]="5";
//		String[] amount = new String[3];
//		amount[0]="2";
//		amount[1]= "2";
//		amount[2]="2";
//		
//		
//		String response1 =DAO.insertPurchase("1", amount, "8", merchid);
//		
//		System.out.println(response1);
//		System.out.println("------------------");
//		
//		String[] answer = response1.split(Pattern.quote("$"));
//		for (int i = 0; i < answer.length; i++) {
//			System.out.println(answer[i]);
//		}
//		System.out.println(answer.length);
//		
//		List<PurchaseProductsBean> shops = DAO.ShopProductList("1");
//		int i = shops.size();
//		for (int j = 0; j < i; j++) {
//		System.out.println(shops.get(j).getMerchandiseName());
//		}
}



