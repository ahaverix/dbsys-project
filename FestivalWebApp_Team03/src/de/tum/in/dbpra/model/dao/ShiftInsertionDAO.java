package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ShiftInsertionDAO extends AbstractDAO{
	public void assignShift(int personnel_id, int shift_id) throws SQLException {
		//Query to (re)assign a shift
		String queryReassign = "UPDATE shift SET assigned_to = ? WHERE shift_id = ?;";
		//Query to unassign a shift
		String querySetNull = "UPDATE shift SET assigned_to = null WHERE shift_id = ?;";
		
		//Get connection and start transaction
		Connection con = getConnection();
		con.setAutoCommit(false);
		
		PreparedStatement pstmt;
	
		//Prepare a statement with the query for unassignment
		if(personnel_id == 0){
			pstmt = con.prepareStatement(querySetNull);
			pstmt.setInt(1, shift_id);
		}
		//Prepare a statement with the query for (re)assignment
		else {
			pstmt = con.prepareStatement(queryReassign);
			pstmt.setInt(1, personnel_id);
			pstmt.setInt(2, shift_id);
		}
		
		//get the affected lines and close the statement
		int linesAffected = pstmt.executeUpdate();
		pstmt.close();
		
		//Check if only exactly one line is affected. Otherwise rollback and throw an exception
		if(linesAffected != 1){
			con.rollback();
			con.close();
			throw new SQLException("Update Unsuccessfull: Affected Rows does not equal 1 ("+linesAffected+")");		
		}
		
		//Commit and close the connection
		con.commit();
		con.close();
	}
}
