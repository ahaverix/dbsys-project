package de.tum.in.dbpra;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de.tum.in.dbpra.model.bean.LoginBean;
import de.tum.in.dbpra.model.dao.LoginDAO;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/*// for testing
	private final String email = "carol.meyer@tum.de";
	private final String password = "qwert";*/
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get request parameters for user email and password
		String user = request.getParameter("user");
		String pwd = request.getParameter("pwd");
					
		try {			
			LoginDAO dao = new LoginDAO();
        	LoginBean login = new LoginBean();
			dao.getUser(user, login);
			//getting important information related to the user, needed for login and session attributes
			request.setAttribute("loginID", login.getPerson_id());
			request.setAttribute("loginName", login.getName());
			request.setAttribute("loginEmail", login.getEmail());
			request.setAttribute("loginPassword", login.getPassword());
		} catch (Throwable e) {
    		e.printStackTrace();
    		request.setAttribute("error", e.getMessage());
    	}
	
		/*
		 * if(request.getParameter("login")==null){ throw new
		 * MissingInputException("ERROR: No Login information!"); }
		 */
		
		String email = String.valueOf(request.getAttribute("loginEmail"));
		String password = String.valueOf(request.getAttribute("loginPassword"));
		String username = String.valueOf(request.getAttribute("loginName"));
		int loginID = (int) request.getAttribute("loginID");

		if (email.equals(user) && password.equals(pwd)) {
			//if login succeeds a session is created, and several attributes are set (name, email, id)
			HttpSession session = request.getSession();
			session.setAttribute("user", username);
			session.setAttribute("email", email);
			session.setAttribute("loginID", loginID);
			// setting session to expire in 30 mins
			// session.setMaxInactiveInterval(30 * 60);
			//also using cookies as an alternate form of accessing data
			Cookie userEmail = new Cookie("user", user);
			// userEmail.setMaxAge(30 * 60);
			response.addCookie(userEmail);
			response.sendRedirect("LoginSuccess.jsp");
		} else {
			//if the login fails due to incorrect username or password, the user is given appropriate feedback 
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/loginfailed.html");
			/*PrintWriter out = response.getWriter();
			out.println("<font color=red>Either user name or password is wrong.</font>");*/
			rd.include(request, response);
		}

	}

}
