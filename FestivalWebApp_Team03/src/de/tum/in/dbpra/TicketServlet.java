package de.tum.in.dbpra;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.bean.TicketBean;
import de.tum.in.dbpra.model.dao.TicketDAO;


@WebServlet("/booking")
public class TicketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TicketServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/book_ticket.jsp");
		dispatcher.forward(request, response);
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException, NumberFormatException {
		try {
        	TicketDAO dao = new TicketDAO();
        	TicketBean ticket = new TicketBean();
			
			//set ticket attributes
        	ticket.setType(request.getParameter("type"));
			ticket.setArea(request.getParameter("area"));
			
			int vip = -1;
			ticket.setVip(vip);
			
        	if(request.getParameter("vip") != null && request.getParameter("area") != null){
        		vip =  Integer.parseInt(request.getParameter("vip"));
        		ticket.setVip(vip);
        		ticket.setValidFrom();
    			ticket.setPrice();
        	}
        	
			
			//get email of the logged in user		
        	String clientEmail = (String)request.getSession().getAttribute("email");
        	
        	//insert ticket
        	dao.insertTicket(ticket, clientEmail);
        	request.setAttribute("ticket", ticket);
        	
        	//send attributes to jsp
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/book_ticket.jsp");
    		dispatcher.forward(request, response);
        	
    	} catch (Throwable e) {
    		request.setAttribute("error", e.getMessage());
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/book_ticket.jsp");
    		dispatcher.forward(request, response);
    	}
		
		
	}
	

}
