package de.tum.in.dbpra;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.bean.*;
import de.tum.in.dbpra.model.dao.*;

/**
 * Servlet implementation class AssignToExistingServlet
 */
@WebServlet("/assignToExisting")
public class AssignToExistingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AssignToExistingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/unpersonalized_tickets.jsp");
		dispatcher.forward(request, response);
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			PersonalizeDAO dao = new PersonalizeDAO();
			
			//get e-mail of the logged in user and of the visitor to whom the ticket will be assigned
			String visitor_email = request.getParameter("email");
			String clientEmail = (String)request.getSession().getAttribute("email");
			
			//get booked tickets.
        	ArrayList<TicketBean> tickets;
			tickets = dao.getUnpersonalizedTickets(clientEmail);
			request.setAttribute("tickets", tickets);
			
			//assignment
			int ticket_id = -1;
			if(request.getParameter("ticket_id").matches("[0-9]+")){
        		ticket_id =  Integer.parseInt(request.getParameter("ticket_id"));
        	}
			dao.personaizeTicket(ticket_id, visitor_email, clientEmail);
			request.setAttribute("assignment", "assignment ok");
			
			//update the ticket list
			tickets = dao.getUnpersonalizedTickets(clientEmail);
			request.setAttribute("tickets", tickets);
			
			
			//send attr to jsp
			RequestDispatcher dispatcher = request.getRequestDispatcher("/unpersonalized_tickets.jsp");
			dispatcher.forward(request, response);
			
    	} catch (Throwable e) {
    		request.setAttribute("error", e.getMessage());
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/unpersonalized_tickets.jsp");
    		dispatcher.forward(request, response);
    	}
		
		
	}

}
