﻿-- Development database (based on UML Class Diagram)

create table person(
  person_id    serial primary key,
  first_name     varchar(35) not null,
  last_name   varchar(35) not null,
  gender      char(1) not null check(gender = 'M' or gender = 'F'),
  title     varchar(15),
  birthday  date not null,
  phone_no       varchar(20),
  email varchar(40) unique not null,
  address     varchar(60),
  password varchar(20) not null
);

create table Merchandise(
    merchandise_id serial primary key,
    name varchar(30),
    provider varchar(8) not null check (provider = 'festival' or provider = 'band' or provider = 'sponsor'),
    type varchar(20),
    quantity int not null check(quantity >= 0),
    price money not null
);

CREATE TABLE organizer(
    person_id integer PRIMARY KEY REFERENCES person(person_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    task VARCHAR(50) NOT NULL,
    subordinates_no smallint NOT NULL DEFAULT 0 check(subordinates_no >= 0),
    salary MONEY NOT NULL
);

CREATE TABLE personnel(
    person_id integer PRIMARY KEY REFERENCES person(person_id)
         ON DELETE CASCADE ON UPDATE CASCADE,
    job varchar(15) NOT NULL,
    salary MONEY NOT NULL,
    experience VARCHAR(20),
    self_insured BOOLEAN NOT NULL
);

create table client(
  person_id integer primary key references person (person_id) on update cascade on delete cascade,
  credit_card_no   char(16) not null,
  expiration_date   date not null check(expiration_date  >= current_date),
  is_company  boolean not null,
  company_name     varchar(50)
);

create table visitor(
  person_id integer primary key references person (person_id) on update cascade on delete cascade,
  disability  varchar(25)
);

create table AccSponsor(
    sponsor_id serial primary key,
    sponsor_name varchar(20) not null,
    contact_no varchar(20) unique not null,
    email varchar(30) unique not null,
    contact_person varchar(30) not null,
    payment money not null,
    type varchar(10) not null
);

create table AccBand(
    band_id serial primary key,
    band_name varchar(30) unique not null,    
    members_list varchar(150),
    sells_merchandise boolean not null,
    performances smallint not null check (performances > 0 and performances < 4),
    instructions varchar(500),
    press_info text,
    cost money not null
);

CREATE TABLE note(
    note_id SERIAL PRIMARY KEY,
    content VARCHAR(300) NOT NULL,
    posted_at TIMESTAMP NOT NULL,
    posted_by integer NOT NULL REFERENCES organizer(person_id)
ON DELETE RESTRICT ON UPDATE CASCADE
);

create table ticket(
  ticket_id serial primary key,
  type varchar(10) not null check(type='friday' or type='weekend' or type='saturday' or type= 'sunday'),
  valid_from date not null,
  area varchar(10) not null check(area = 'concert' or area = 'camping' or area = 'both'),
  vip boolean not null default false,
  client_id integer not null references client(person_id) on update cascade on delete cascade,
  visitor_id integer references visitor(person_id) on update cascade on delete cascade,
  price money not null check(price>=money(0)),
  booking_time TIMESTAMP NOT NULL
);

create table wristband(
  wristband_id serial primary key,
  valid_from date not null,
  valid_to date not null,
  vip boolean not null default false,
  current_balance money not null check(current_balance>=money(0)),
  ticket_id integer references ticket (ticket_id) on update cascade on delete set null
);

CREATE TABLE location(
location_id SERIAL PRIMARY KEY,
name VARCHAR(20) UNIQUE NOT NULL,
type VARCHAR(20) NOT NULL,
billboards_no SMALLINT NOT NULL CHECK(billboards_no >= 0),
size smallint NOT NULL check(size > 0),
latitude DECIMAL(8,6) NOT NULL,
longitude DECIMAL(9,6) NOT NULL,
area varchar(10) NOT NULL check(area = 'concert' or area = 'camping') 
);

CREATE TABLE shift(
    shift_id SERIAL PRIMARY KEY,
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    task VARCHAR(50) NOT NULL,
    assigned_to integer REFERENCES personnel(person_id)
         ON DELETE SET NULL ON UPDATE CASCADE,
executed_in integer NOT NULL REFERENCES location(location_id)
ON DELETE RESTRICT ON UPDATE CASCADE
);

create table Shop(
    shop_id serial primary key,
    shop_name varchar(20),
    type boolean not null default false,
    shop_size decimal(6,2) not null,
    cashbox_amount money not null,
    situated_in integer not null unique references location(location_id)
        on delete restrict on update cascade
);

create table inventory(
    shop_id integer references shop(shop_id)
        on update cascade on delete cascade,
    merchandise_id integer references merchandise(merchandise_id)
        on update cascade on delete cascade,
    amount integer not null default 0 check(amount >=0),
    primary key(shop_id, merchandise_id)
);

create table contains_billboards(
    location_id integer references location(location_id)
        on update cascade on delete cascade,
    sponsor_id integer not null references accsponsor(sponsor_id)
        on update cascade on delete cascade,
    quantity smallint not null, --make sure sum of all quantities related to one location <= location.billboards_no
    primary key(location_id, sponsor_id)
);

create table sells(
    merchandise_id integer references merchandise(merchandise_id)
        on update cascade on delete cascade,
    sponsor_id integer references accsponsor(sponsor_id)
        on update cascade on delete cascade,
    primary key(merchandise_id, sponsor_id)
);

create table sells_in(
    sponsor_id integer references accsponsor(sponsor_id)
        on update cascade on delete cascade,
    shop_id integer references shop(shop_id)
        on update cascade on delete cascade,
    employees_no smallint not null,
primary key(sponsor_id, shop_id)
);
    
create table provides(
    band_id integer references accband(band_id)
        on update cascade on delete cascade,
    merchandise_id integer references merchandise(merchandise_id)
        on update cascade on delete cascade,
    primary key(band_id, merchandise_id)
);

create table plays(
    band_id integer references accband(band_id)
        on update cascade on delete cascade,
    location_id integer references location(location_id)
        on update cascade on delete cascade,
    instructions varchar(500) not null,
    start_buildup timestamp not null,
    start_play timestamp not null,
    end_play timestamp not null,
    end_teardown timestamp not null,
    primary key(band_id, location_id)
);

CREATE TABLE chooses(
    visitor_id integer REFERENCES visitor(person_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    band_id integer REFERENCES accband(band_id)
ON DELETE CASCADE ON UPDATE CASCADE,
PRIMARY KEY (visitor_id, band_id)
);

CREATE TABLE purchase(
    wristband_id integer REFERENCES wristband(wristband_id)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    merchandise_id integer REFERENCES merchandise(merchandise_id)
ON DELETE RESTRICT ON UPDATE CASCADE,
    shop_id integer REFERENCES shop(shop_id)
        ON DELETE RESTRICT ON UPDATE CASCADE,
time TIMESTAMP,
quantity SMALLINT NOT NULL DEFAULT 1 CHECK (quantity > 0),
PRIMARY KEY (time, wristband_id, merchandise_id, shop_id)    
);

CREATE TABLE gets_note(
    personnel_id integer REFERENCES personnel(person_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    note_id integer REFERENCES note(note_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (personnel_id, note_id)
);

-- Indexes

create index index_person_name on person(last_name, first_name);
create index index_appsponsor_sponsor_name on AccSponsor(sponsor_name);
create index index_note_posted_at on note(posted_at);
create index index_shift_assigned_to  on shift(assigned_to);
create index index_location_lat_long  on location(latitude, longitude); 
create index index_plays_start_play on plays(start_play); 
create index index_ticket_type on ticket(type);

-- Triggers

-- trigger for max no of tickets bought by client (20)
CREATE OR REPLACE FUNCTION enforce_books_count()
  RETURNS trigger AS
$BODY$
declare
    max_ticket_count integer := 20; --test and replace with 20
    ticket_count integer := 0;
    to_check boolean := false; -- we check for all inserts and only some updates
begin
    if tg_op = 'INSERT' then --tg_op is a keyword that takes values insert, update, delete, truncate
        to_check := true;
    end if;
    if tg_op = 'UPDATE' then
        if (new.client_id != old.client_id) then
            to_check := true;
        end if;
    end if;
    if to_check then
        -- prevent concurrent inserts from multiple transactions
        lock table ticket in exclusive mode;
        
        select into ticket_count count(*)
        from ticket
        where client_id = new.client_id;

        if ticket_count >= max_ticket_count then
            raise exception 'Cannot insert more than % tickets per client.', max_ticket_count;
        end if;
    end if;
    return new;
end;
$BODY$
  LANGUAGE plpgsql;

CREATE TRIGGER enforce_books_count
  BEFORE INSERT OR UPDATE
  ON ticket
  FOR EACH ROW
  EXECUTE PROCEDURE enforce_books_count();

-- Trigger for purchase - updating the values of inventory amount and wristband balance on insertion or deletion of a purchase - two triggers in one 
CREATE OR REPLACE FUNCTION update_inventory_balance_onpurchase()
  RETURNS trigger AS
$BODY$
declare
    varquantity integer := 0;
    varinventory integer := 0;
    varprice decimal := 0.0;
    varbalance decimal := 0.0;
begin
    lock table purchase in exclusive mode;
    lock table inventory in exclusive mode;
    lock table wristband in exclusive mode;

    if tg_op = 'INSERT' then 
        varquantity = new.quantity;

        -- LOGIC INVENTORY
        select into varinventory i.amount
        from inventory i
        where i.shop_id=new.shop_id and new.merchandise_id=i.merchandise_id;
        raise notice 'Inventory is %.', varinventory;
    
        if (varquantity > varinventory) then
            raise exception 'Cannot make the purchase, not enough items in inventory';
        end if;

        varinventory = varinventory - varquantity;
        update inventory set amount=varinventory where shop_id=new.shop_id and merchandise_id=new.merchandise_id; 
        raise notice 'Inventory amount decreased by % items.', varquantity;

        -- LOGIC BALANCE
        select into varbalance w.current_balance::money::numeric 
        from wristband w
        where w.wristband_id=new.wristband_id;
        raise notice 'Balance is %.', varbalance;

        select into varprice m.price::money::numeric
        from merchandise m
        where m.merchandise_id=new.merchandise_id;
        raise notice 'Price is %.', varprice;

        if (varquantity*varprice > varbalance) then
            raise exception 'Cannot make the purchase, not enough credit in wristband';
        end if;

        varbalance = varbalance - varquantity*varprice;
        update wristband set current_balance=money(varbalance) where wristband_id=new.wristband_id; 
        raise notice 'Wristband balance decreased by %.', varquantity*varprice;

        return new;
    end if;
    
    if tg_op = 'UPDATE' then
        raise exception 'No update allowed on purchase. Only insert and delete ;)';
    end if;

    if tg_op = 'DELETE' then
        varquantity = old.quantity;

        -- LOGIC INVENTORY
        select into varinventory i.amount
        from inventory i, purchase p
        where i.shop_id=p.shop_id and p.merchandise_id=i.merchandise_id 
            and p.merchandise_id=old.merchandise_id and p.shop_id=old.shop_id;
        raise notice 'Inventory is %.', varinventory;

        varinventory = varinventory + varquantity;
        update inventory set amount=varinventory where shop_id=old.shop_id and merchandise_id=old.merchandise_id; 
        raise notice 'Inventory amount increased by % items.', varquantity;

        -- LOGIC BALANCE
        select into varbalance w.current_balance::money::numeric 
        from wristband w, purchase p
        where w.wristband_id=p.wristband_id and p.wristband_id=old.wristband_id;
        raise notice 'Balance is %.', varbalance;

        select into varprice m.price::money::numeric
        from merchandise m, purchase p
        where m.merchandise_id=p.merchandise_id and p.merchandise_id=old.merchandise_id;
        raise notice 'Price is %.', varprice;

        varbalance = varbalance + varquantity*varprice;
        update wristband set current_balance=money(varbalance) where wristband_id=old.wristband_id; 
        raise notice 'Wristband balance increased by %.', varquantity*varprice;

        return old;    
    end if;
end;
$BODY$
  LANGUAGE plpgsql;
  
CREATE TRIGGER update_inventory_balance_onpurchase
  BEFORE INSERT OR UPDATE OR DELETE
  ON purchase
  FOR EACH ROW
  EXECUTE PROCEDURE update_inventory_balance_onpurchase();

-- trigger that makes sure that there are max. 5 shops per sponsor in sells_in
CREATE OR REPLACE FUNCTION limit_shops_per_sponsor()
  RETURNS trigger AS
$BODY$
declare
    max_shops integer := 5;
    shop_count integer := 0;
    to_check boolean := false; 
begin
    if tg_op = 'INSERT' then 
        to_check := true;
    end if;
    if tg_op = 'UPDATE' then
        if (new.sponsor_id != old.sponsor_id) then
            to_check := true;
        end if;
    end if;
    if to_check then
        -- prevent concurrent inserts from multiple transactions
        lock table sells_in in exclusive mode;
        
        select into shop_count count(*)
        from sells_in
        where sponsor_id = new.sponsor_id;

        if shop_count >= max_shops then
            raise exception 'Cannot insert more than % shops per sponsor.', max_shops;
        end if;
    end if;
    return new;
end;
$BODY$
  LANGUAGE plpgsql;

CREATE TRIGGER limit_shops_per_sponsor
  BEFORE INSERT OR UPDATE
  ON sells_in
  FOR EACH ROW
  EXECUTE PROCEDURE limit_shops_per_sponsor();

-- trigger that makes sure that a personnel is not assigned to multiple shifts at overlapping times
CREATE OR REPLACE FUNCTION prohibit_overlapping_shifts()
  RETURNS trigger AS
$BODY$
declare
    to_check boolean := false; 
    overlaps_with integer := 0;
begin
    if tg_op = 'INSERT' then 
        to_check := true;
    end if;
    if tg_op = 'UPDATE' then
        if (new.assigned_to != old.assigned_to) then
            to_check := true;
        end if;
    end if;
    if to_check then
        -- prevent concurrent inserts from multiple transactions
        lock table shift in exclusive mode;
        
        select into overlaps_with count(*)
        from shift
        where assigned_to=new.assigned_to and ((start_time >= new.start_time AND start_time < new.end_time)
OR(end_time > new.start_time AND end_time <= new.end_time)
OR(start_time <= new.start_time AND end_time >= new.end_time));

        if overlaps_with > 0 then
            raise exception 'The shift overlaps with another shift of that personnel';
        end if;
    end if;
    return new;
end;
$BODY$
  LANGUAGE plpgsql;

CREATE TRIGGER prohibit_overlapping_shifts
  BEFORE INSERT OR UPDATE
  ON shift
  FOR EACH ROW
  EXECUTE PROCEDURE prohibit_overlapping_shifts();

--Limits the accepted bands to 30
CREATE OR REPLACE FUNCTION enforce_accBand_count()
RETURNS Trigger AS 
$BODY$
declare
    accBandcount INTEGER := 0;
    must_check BOOLEAN :=0;
    max_accBand  INTEGER:= 30; -- change for testing 
BEGIN
    IF TG_OP = 'INSERT' THEN
    must_check := true;
        END IF;
    IF must_check THEN
        -- prevent concurrent inserts from multiple transactions
        LOCK TABLE accBand IN EXCLUSIVE MODE;

          SELECT COUNT(*) 
          INTO accBandcount 
          FROM accBand;

        if accBandcount >= Max_AccBand then
            raise exception 'All available Artist slots have been filled.';
        end if;

    END IF;

    RETURN NEW;
END
$BODY$
  LANGUAGE plpgsql;

--Trigger for the maximum daily tickets (200k per day) 
CREATE OR REPLACE FUNCTION enforce_daily_ticket_count()
RETURNS Trigger AS 
$BODY$

Declare
    total_weekend_tickets INTEGER := 0; 
    total_friday_tickets INTEGER := 0;
    total_saturday_tickets INTEGER := 0;
    total_sunday_tickets INTEGER := 0;
    must_check BOOLEAN :=0;
    maxNr INTEGER:= 200000; --change for testing
BEGIN
-- early lock to ensure correct values for the count(*)
        LOCK TABLE ticket IN EXCLUSIVE MODE;
        total_weekend_tickets := (select count(*) from ticket where type = 'weekend'); 
    total_friday_tickets := (select count(*) from ticket where type = 'friday');
    total_saturday_tickets := (select count(*) from ticket where type = 'saturday');
    total_sunday_tickets := (select count(*) from ticket where type = 'sunday');
    
    IF TG_OP = 'INSERT' THEN
    must_check := true;
    END IF;
    IF TG_OP = 'UPDATE' THEN 
        if (new.type != old.type) then
            must_check := true;
        END IF;
    END IF;

    IF must_check THEN        
        IF new.type = 'friday' THEN
              IF total_weekend_tickets+total_friday_tickets >= maxNr then
                  raise exception 'We are sorry, no Friday tickets left.';
              END IF;
        END IF;
        IF new.type = 'saturday' THEN
            IF total_weekend_tickets+total_saturday_tickets >= maxNr then
                  raise exception 'We are sorry, no Saturday tickets left.';
              END IF;
          END IF;    
          IF new.type = 'sunday' THEN
            IF total_weekend_tickets+total_sunday_tickets >= maxNr then
                  raise exception 'We are sorry, no Sunday tickets left.';
              END IF;
          END IF;        
          IF new.type = 'weekend' THEN
            IF total_weekend_tickets+ GREATEST(total_friday_tickets,total_saturday_tickets,total_sunday_tickets) >= maxNr then
                  raise exception 'We are sorry, no Weekend tickets left.';
              END IF;    
          END IF;    
    END IF;

    RETURN NEW;
END
$BODY$
  LANGUAGE plpgsql;

CREATE TRIGGER enforce_daily_ticket_count
  BEFORE INSERT OR UPDATE
  ON ticket
  FOR EACH ROW
  EXECUTE PROCEDURE enforce_daily_ticket_count();




