-----------
--visitors
-----------
 
--1. If the wristband 12 is lost, unassign the lost one and reassign a new one to the visitor with the same information
 
insert into wristband (valid_from, valid_to, vip, current_balance, visitor_id)
  select valid_from, valid_to, vip, current_balance, visitor_id
  from wristband
  where wristband_id=12;
update wristband set visitor_id = null where wristband_id=12;
 

--2. Get timetable for visitor (chosen bands, times, place)
 
with bands_schedule (bands_schedule) as
(
	select * from appband left join plays on appband.band_id=plays.band_id
)
select visitor_id, band_name, location_id, start_play, end_play from chooses left join bands_schedule on chooses.band_id = bands_schedule.band_id  
where visitor_id=1;

 
--3. Perform a purchase (using the triggers)
 
select * from inventory where shop_id=7 and merchandise_id=2;
insert into purchase values(8,2,7,now(),1);
select * from inventory where shop_id=7 and merchandise_id=2;
 

--4. How many tickets did a client book so far and how many can be still booked
 
select count(*) as booked_tickets, 20-count(*) as remaining_tickets
from tickets
where client_id=20;

--5. Sponsor merchandise and where it can be bought

select m.name, sh.shop_name, l.location_id, l.latitude, l.longitude
from location l, sells_in si, shop sh, accsponsor acs, appsponsor aps, sells s, merchandise m
where l.location_id = sh.situated_in and sh.shop_id = si.shop_id and si.sponsor_id = acs.sponsor_id and aps.sponsor_name = 'Heineken'
and acs.sponsor_id = aps.sponsor_id and s.merchandise_id = m.merchandise_id and aps.sponsor_id = s.sponsor_id;
 


-----------
-- providors
-----------

--1. Location of sponsor shop

select sh.shop_name, l.location_id, l.latitude, l.longitude
from location l, sells_in si, shop sh, accsponsor acs, appsponsor aps
where l.location_id = sh.situated_in and sh.shop_id = si.shop_id and si.sponsor_id = acs.sponsor_id and aps.sponsor_name = 'Heineken'
and acs.sponsor_id = aps.sponsor_id;

--This query will be used a considerable amount of times for example when people want to see at which location they can find the merchandise they need.


--2. Total amount of profit for sponsor.

select sum(m.price) as Total_Amount_Of_Profit
from merchandise m, shop sh, purchase p, appsponsor aps, sells_in si, sells s
where aps.sponsor_id = si.sponsor_id and sh.shop_id = si.shop_id and 
aps.sponsor_name = 'Red Bull' and s.merchandise_id = m.merchandise_id and p.merchandise_id = m.merchandise_id and p.shop_id = sh.shop_id;

-- 3. Find the quantity of a specific service / product in each shop

select i.shop_id, shop_name, amount
from inventory i, shop s, merchandise m
where i.shop_id=s.shop_id and i.merchandise_id = m.merchandise_id and i.merchandise_id = 3;
--checking for availability of water in each shop for logistic purposes

--4. Compute the sum of purchases made in each shop in the current day

select p.shop_id, shop_name, sum(p.quantity*m.price) as total
from purchase p, shop s, merchandise m
where p.shop_id=s.shop_id and p.merchandise_id=m.merchandise_id and date(time) = current_date --2016-06-21 to test
group by p.shop_id, shop_name;
--insert into purchase values(8, 10, 4, '2016-06-22 19:02:01', 2);

--5. Find the most successful products

with purchases(merchandise_id, name, provider, sum_items) as
(
select m.merchandise_id, m.name, provider, sum(p.quantity) as sum_items
from purchase p, merchandise m
where p.merchandise_id = m.merchandise_id
group by m.merchandise_id
)    
select merchandise_id, name, provider, sum_items as sold_items
from purchases
order by sum_items desc limit 3;



-------------
-- organization
-------------

-- 1. workload of all personnel (ordered by workload)
 
SELECT p.person_id, SUM(end_time - start_time) AS workload
FROM personnel p  
    JOIN shift s ON  
   	 p.person_id = s.assigned_to
GROUP BY p.person_id 
ORDER BY workload DESC;
 

-- 2. emails of all journalists/visitors/clients that did not subscribe to a newsletter yet

SELECT distinct email
FROM (  
    (SELECT email
    FROM person p
   	 JOIN journalist j ON
   		 p.person_id = j.person_id
   	 FULL OUTER JOIN j_subscribes_for js ON
   		 p.person_id = js.journalist_id
    WHERE js.journalist_id IS NULL)
UNION
    (
    SELECT email
    FROM person p
   	 JOIN visitor v ON
   		 p.person_id = v.person_id
   	 FULL OUTER JOIN v_subscribes_for vs ON
   		 p.person_id = vs.visitor_id
    WHERE vs.visitor_id IS NULL
    )  
UNION
    (
    SELECT email
    FROM person p
   	 JOIN client c ON
   		 p.person_id = c.person_id
   	 FULL OUTER JOIN c_subscribes_for cs ON
   		 p.person_id = cs.client_id
    WHERE cs.client_id IS NULL
    )
) p_no_sub;
 

 

--3. visitors that are currently in the concert area
 
SELECT p.*
FROM entered e  
	JOIN wristband w ON
    	e.wristband_id = w.wristband_id
	JOIN visitor v ON
    	w.visitor_id = v.person_id
	JOIN person p ON
 	v.person_id = p.person_id
WHERE e.area_name = 'concert' AND
	NOT EXISTS(
    	SELECT *
    	FROM entered e2
    	WHERE e2.area_name <> e.area_name
        	AND e2.time > e.time
);
 
 
 

--4. Find personnel working right now

select p.first_name, p.last_name, p.phone_no, w.experience, s.task, l.name, l.type
from person p 
  join personnel w on
     p.person_id = w.person_id 
  JOIN shift s ON
     s.assigned_to = p.person_id
  JOIN location l ON
     s.executed_in = l.location_id 
where s.start_time <= now() and s.end_time > now();


--5. Compute the sum of interested visitors in each band - interesting for allocating and preparing the stage locations (security, med)

select band_name, count(*) interested_visitors
from chooses c, accband a, appband ap
where c.band_id = a.band_id and a.band_id=ap.band_id
group by ap.band_id;


-- extra query: Identify the current connected users,
-- applications, open ports and user privileges
 
select s.usename, s.application_name, s.client_addr, s.client_port, s.usesysid, u.usecreatedb, u.usesuper, u.usecatupd 
from pg_stat_activity s, pg_user u
where s.usesysid=u.usesysid; 

